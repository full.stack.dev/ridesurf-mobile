<p align="center"><img src="https://ridesurf.io/images/ridesurf.png"></p>


## About Ridesurf

Ridesurf is a long-distance ride sharing app. It’s a safe and easy way for two people or more who are going the same direction to share a ride with one another. It works best for road trips and drives that are pre-planned. 

<strong>Ridesurf V1.0 Beta</strong>

## IONIC 3 App

Current Build: 
   -ionic (Ionic CLI)  : 4.10.3 
   Ionic Framework    : ionic-angular 3.9.3
   -@ionic/app-scripts : 3.2.4


## Process for deplyoing: 

1.- ionic cordova platform rm ios
2.- ionic cordova platform add ios
3.- npm i @ionic/app-scripts@3.2.3
4.- increase version # in config file and on General Identity tab in xCode
5.- xCode: Make sure the build info as the corresponding strings (below) 
6.- xCode: Check Build Settings > Signing and for "Code Signing Identity" hit "multiple values" and select "iOS Developer"
7.- xCode: Make sure "Generic iOS device" is selected in the Simulator drop down
8.- xCode: hit "Signing & Capabilities" tab and make sure "Travlr Co" is selected as dev team
9.- Build in terminal:  ionic cordova build ios --prod --release
10.- Xcode top menu: Project > Archive then hit "Distribute App" and select all default settings
11.- Go to App Store Connect, create new version, add in promotional text from last version, add in update release notes, select new build (it will not show up until its done processing) then hit save & submit

Strings: 
NSCameraUsageDescription - Ridesurf uses your camera in order to upload a user photo and vehicle photos.
NSPhotoLibraryUsageDescription - Ridesurf access your photo library in order to upload a user photo and vehicle photos.
NSLocationWhenInUseUsageDescription & NSLocationAlwaysUsageDescription - Ridesurf would like to access your location in order to perform ride searches


## Fix Build Issues

1.- ionic cordova platform rm ios
2.- ionic cordova platform add ios
3.- npm i @ionic/app-scripts@3.2.3
4.- ionic cordova build ios
5.- ionic cordova plugin add cordova-plugin-facebook4@5.0.0 --variable APP_ID="320388125222700" --variable APP_NAME="Ridesurf"

ionic cordova plugin add cordova-plugin-facebook4 --save --variable APP_ID="320388125222700" --variable APP_NAME="Ridesurf"

