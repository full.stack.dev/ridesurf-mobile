import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the PassengerReviewsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'passenger-reviews',
  templateUrl: 'passenger-reviews.html'
})
export class PassengerReviewsComponent {

  
  users:any;
  url:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
  			  public app:App,  public viewCtrl: ViewController, public global:GlobalProvider) {
  	this.users = this.navParams.get('user_rides');
  	this.url = this.global.url;
    
  }

  review(user:any){
  	this.viewCtrl.dismiss();
  	this.app.getRootNav().setRoot('ReviewPassengerPage', {user: user});
  }

}
