import { NgModule ,  NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { NotValidatedComponent } from './not-validated/not-validated';
import { UserRatingComponent } from './user-rating/user-rating';
import { VerifiedInfoComponent } from './verified-info/verified-info';
import { MissingInfoComponent } from './missing-info/missing-info';
import { CreateMessageComponent } from './create-message/create-message';
import { PassengerReviewsComponent } from './passenger-reviews/passenger-reviews';


import { CommonModule  } from '@angular/common';  
import { StarRatingModule } from 'ionic3-star-rating';
import { IonicStorageModule } from '@ionic/storage';
import { IonicModule } from 'ionic-angular';
import { IonicApp } from 'ionic-angular';
import { FilterComponent } from './filter/filter';
import { RideRemindersComponent } from './ride-reminders/ride-reminders';
import { ParkedComponent } from './parked/parked';
import { PickUpComponent } from './pick-up/pick-up';

@NgModule({
	declarations: [
        NotValidatedComponent,
        UserRatingComponent,
        VerifiedInfoComponent,
        MissingInfoComponent,
        CreateMessageComponent,
        PassengerReviewsComponent,
        FilterComponent,
        RideRemindersComponent,
    ParkedComponent,
    PickUpComponent
    ],
	imports: [
        IonicModule,
        CommonModule, 
        StarRatingModule,
        IonicStorageModule,
    ],
    entryComponents:[
        NotValidatedComponent,
        UserRatingComponent,
        VerifiedInfoComponent,
        MissingInfoComponent,
        CreateMessageComponent,
        PassengerReviewsComponent,
        FilterComponent,
        RideRemindersComponent,
    ],
	exports: [
        NotValidatedComponent,
        UserRatingComponent,
        VerifiedInfoComponent,
        MissingInfoComponent,
        CreateMessageComponent,
        PassengerReviewsComponent,
        FilterComponent,
        RideRemindersComponent,
    ParkedComponent,
    PickUpComponent
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
})
export class ComponentsModule {}
