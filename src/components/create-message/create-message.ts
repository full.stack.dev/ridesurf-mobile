import { Component,  } from '@angular/core';
import { SingleMessagePage } from '../../pages/single-message/single-message';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { AlertController, ToastController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { LoadingController, ViewController, NavController, App, NavParams } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";

/**
 * Generated class for the CreateMessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'create-message',
  templateUrl: 'create-message.html'
})
export class CreateMessageComponent {

  userContacts:any;
  ride:any;
  body:any;
  otherUser:any;
  ride_id:any;
  phoneChange:any; 
  emailChange:any;
  beforePay:boolean = false;
  rider:boolean;
  currentUser:any;
  constructor(public global : GlobalProvider, public loadingCtrl: LoadingController, public app:App,
  	public http: HttpClient, public alertCtrl:AlertController, public viewCtrl: ViewController,
  	public navCtrl: NavController, public toastCtrl: ToastController, public params: NavParams,
    public userProvider:UserProvider) {

    this.otherUser = this.params.get('otherUser');
    this.ride_id = this.params.get('ride_id');
    this.rider  = this.params.get('rider');
    this.currentUser = this.userProvider.currentUser();
    console.log(this.rider);
    this.phoneChange = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/;
    this.emailChange =  /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;

    if(this.otherUser && this.ride_id){
      this.ride = this.ride_id+","+this.otherUser;
    }

  	let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

    loader.present();

    if(this.rider){
          var link = this.global.urlApi + "/getSingleUserContact";
          var headers = new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token'));

    this.http.post(link, {
        rider:this.rider,
        passenger_id:this.otherUser,
        rides_id:this.ride_id
      }, 
      {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
      
      }).subscribe(data => {
        this.userContacts = data;
        if(this.userContacts.booker_payment_paid == 'No'){
           this.beforePay = true;
        }
        console.log(this.userContacts);
        loader.dismiss();
      }, error => {
        console.log('there was an error inside rider');
       console.log(error);
       console.log("Oooops!");
       loader.dismiss();
    });
    }else{
      var link = this.global.urlApi + "/getUserToContact";

      this.http.get(link, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),

        }).subscribe(data => {
          
          this.userContacts = data;
          console.log(this.userContacts);
          loader.dismiss();
        }, error => {
         console.log(error);
         console.log("Oooops!");
         loader.dismiss();
    });
    }

  }

  dismiss() {
    this.viewCtrl.dismiss();
   }


  checkRide(user_ride){
     console.log('We check');
     console.log(user_ride);
     if(user_ride.booker_payment_paid == 'No'){
       this.beforePay = true;
     }
  }


   createMessage(){
    if(this.body == null){
      const toast = this.toastCtrl.create({
          message: 'Message canot be empty',
          duration: 2000,
      });
      toast.present();

      return;
    }

    if(this.beforePay){
      //This is a pre-booking message 
      this.body = this.body.replace(this.phoneChange, '########');
      this.body = this.body.replace(this.emailChange, '*********');
    }

   	if(this.ride == null){
   		let prompt = this.alertCtrl.create({
	        title: 'Error',
	        message: 'Please select a user to message',
	        buttons:
	            [{
	                text: 'Close',
	                handler: data => {

	                }
	            }]
	    	});
        prompt.present();
   		return;
   	}
   	var link = this.global.urlApi + "/storeNewMessage";
   	let loader = this.loadingCtrl.create({
      content: "Sending..."
    });

    loader.present();
   	this.http.post(link,{
   		ride:this.ride_id +","+this.otherUser,
   		body:this.body,
      before_pay:this.beforePay,
      rider:this.rider,
      user_id:this.otherUser
   	},
   	{
    headers: new HttpHeaders()
        .set("Content-Type", "Application/json")
        .set("Accept", "application/json")
        .set('Authorization', 'Bearer '+ localStorage.getItem('token')),

    }).subscribe(data => {
      loader.dismiss();
      this.viewCtrl.dismiss();
      this.app.getRootNav().setRoot('MessageListPage');
    }, error => {
      loader.dismiss();
      console.log(error);
      console.log("Oooops!");
    });
    
   }

}
