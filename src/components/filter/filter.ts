import { Component  } from '@angular/core';
import { SingleMessagePage } from '../../pages/single-message/single-message';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { LoadingController, ViewController, NavController, App, NavParams } from 'ionic-angular';

/**
 * Generated class for the FilterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'filter',
  templateUrl: 'filter.html'
})
export class FilterComponent {

  price:any;
  ladies:boolean = false;
  value:any;
  from:any;
  date:any;
  to:any;
  aiprotRide:any;
  constructor(public global : GlobalProvider, public loadingCtrl: LoadingController, public app:App,
  	public http: HttpClient, public alertCtrl:AlertController, public viewCtrl: ViewController,
  	public navCtrl: NavController, public navParams: NavParams) {
    
	  	this.value = this.navParams.get('price');
	  	this.from = this.navParams.get('from');
	  	this.to = this.navParams.get('to');
	  	this.date = this.navParams.get('date');
      if(this.value == null || this.value == 0){
        this.value = 2;
      }

  }

  dismiss() {
    this.viewCtrl.dismiss();
   }

   decrease(){
   	console.log('decrease');
   	if(this.value > 4){
   		this.value = this.value - 2;
   	}
   }
   increase(){
   	console.log('increase');
   	if(this.value > 3 && this.value < 150){
   		this.value = this.value + 2;
   	}
   }

   filter(){
   	let loader = this.loadingCtrl.create({
        content: "Loading..."
    });

    loader.present();

    
    var link = this.global.urlApi + "/findRideInfo";
    var headers = new Headers();
    
    headers.append('Accept', 'application/json ');

    this.http.post(link,{
        "Headers": headers,
        "from": this.from,
        "to" :  this.to,
        "date" : this.date,
        "women_only" :this.ladies,
        "price":this.value,
      }).subscribe(data => {
        //this.navCtrl.pop();
        loader.dismiss();
        this.viewCtrl.dismiss();
        this.app.getRootNav().setRoot('RideDetailPage', {
         data : data, 
         from:this.from,
         to:this.to,
         date:this.date,
       });
    }, error => {
      loader.dismiss();
       console.log("Oooops!");
    });
   }

}
