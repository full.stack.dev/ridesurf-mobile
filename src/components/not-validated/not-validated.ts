import { Component } from "@angular/core";
import { UserProvider } from "../../providers/user/user";
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the NotValidatedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "not-validated",
  templateUrl: "not-validated.html"
})
export class NotValidatedComponent {
  currentUser: any;
  notValidated: boolean;

  constructor(public userProvider: UserProvider, public toastCtrl: ToastController) {
    this.verifyUser();
  }

  verifyUser() {
    this.currentUser = this.userProvider.currentUser();
    console.log(this.currentUser);
    if (this.currentUser != null) {
      //Not Verified
      if (this.currentUser.verified == 0) {
        this.notValidated = false;
      } else if (this.currentUser.verified == 1) {
        this.notValidated = true;
      }else{
        this.notValidated = false;
      }
    }
  }

  resendEmail() {
    this.userProvider.resendVerificationEmail();
  }

  refreshUser(){

    this.userProvider.updateProfile();
    
    const toast = this.toastCtrl.create({
      message: "User Refreshed",
      duration: 2300
    });
    toast.present();

    setTimeout(()=>{
      this.verifyUser();
    },2500);

    
  }
}
