import { Component } from '@angular/core';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the UserRatingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'user-rating',
  templateUrl: 'user-rating.html'
})
export class UserRatingComponent {

  currentUser:any;
  rating:any = null;
  constructor(public userProvider : UserProvider, public storage:Storage) {
    
    this.storage.get('userData').then((val) => {
          this.currentUser = val;
          this.rating = this.currentUser.user.rating;
    });
   
  }



}
