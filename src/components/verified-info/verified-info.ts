import { Component } from "@angular/core";
import { UserProvider } from "../../providers/user/user";

/**
 * Generated class for the VerifiedInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "verified-info",
	templateUrl: "verified-info.html"
})
export class VerifiedInfoComponent {
	currentUser: any;
	constructor(public userProvider: UserProvider) {
		this.getUser();
	}

	getUser() {
		this.currentUser = this.userProvider.currentUser();
	}
}
