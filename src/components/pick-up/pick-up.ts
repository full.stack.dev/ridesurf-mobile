import { Component, ViewChild, Input } from '@angular/core';
import { Slides, NavController,LoadingController, ToastController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Output, EventEmitter } from '@angular/core'; 
import { RideHistoryPage } from '../../pages/ride-history/ride-history';

import { GlobalProvider } from '../../providers/global/global';
/**
 * Generated class for the PickUpComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pick-up',
  templateUrl: 'pick-up.html'
})
export class PickUpComponent {
  @Input() location:any;
  @Input() airport:any;
  @Input() price:any;
  @Input() duration:any;
  @ViewChild(Slides) slides: Slides;
  @Output() backFromChild = new EventEmitter<string>();
  @Output() hideBottom = new EventEmitter<string>();
  

  text: string;
  picking:boolean = true;
  type:any;
  ride_date:any;
  minDate:any;
  ride_hour:any;
  meeting:any;
  slideNum:1;
  seats:any = 1;
  luggage_amount:any = 1;
  luggage_size:any = "Small";
  description:any;
  firstStep:boolean = true;
  secondStep:boolean = false;
  allowCreate:boolean = false;
  location_from:any;
  location_to:any;
  constructor(public http: HttpClient,  public global : GlobalProvider,public navCtrl: NavController,
              public loadingCtrl: LoadingController,  public toastCtrl: ToastController) {

    this.minDate = new Date().toISOString();
  }

  backParent(){
    this.backFromChild.emit('eventDesc');
  }

  hideParent(){
    console.log('Hiiding');
     this.hideBottom.emit('');
  }

  nextSlide(){
    this.slideNum++;
    this.slides.slideNext();
  }

  backSlide(){
    this.slideNum--;
    this.slides.slidePrev();
  }

  add(number){
    this.price++;
  }
  sub(number){
    this.price--;
  }
  addSeats(){
    if(this.seats < 6)
      this.seats++;
  }
  subSeats(){
    if(this.seats > 1)
      this.seats--;
  }
  addLuggage(){
    if(this.luggage_amount < 6)
      this.luggage_amount++;
  }
  subLuggage(){
    if(this.luggage_amount > 1){
      this.luggage_amount--;
    }
  }

  rideHistory(){
    this.navCtrl.setRoot(RideHistoryPage, {tabIndex: 'currentRides'});
  }

  createRide(){
    this.allowCreate = true;
    if(this.picking){
      this.type = "Picking";
      this.location_from = this.airport.address +', '+this.airport.city + ', ' + this.airport.state + ', USA';
      this.location_to = this.location;
    }else{
      this.type = "Dropping"
      this.location_from = this.location;
      this.location_to = this.airport.address +', '+this.airport.city + ', ' + this.airport.state  + ', USA';
    }

    var ride = {
      location_from: this.location_from,
      location_to: this.location_to,
      description:this.description,
      price:this.price,
      seats:this.seats,
      seats_available:this.seats,
      luggage_amount:this.luggage_amount,
      luggage_size:this.luggage_size,
      dateFrom:this.ride_date,
      startHour:this.ride_hour,
      duration:this.duration,
    }

    if(this.ride_date == null){
      const toast = this.toastCtrl.create({
          message: 'Please add a date to your ride',
          duration: 3500
        });
        toast.present();
        return false;
    }
    if(this.description == null){
        const toast = this.toastCtrl.create({
          message: 'Please add a description to your ride',
          duration: 3500
        });
        toast.present();
        return false;
    }
    if(this.ride_hour == null){
      const toast = this.toastCtrl.create({
          message: 'Please add a time to your ride',
          duration: 3500
        });
        toast.present();
        return false;
    }


    var airportDetail = {
      airport_id: this.airport.id,
      type:this.type,
      pickingm:this.picking,
      terminal:this.meeting,
    }

    var link = this.global.urlApi + "/createAirportRide";
    let loader = this.loadingCtrl.create({
      content: "Creating..."
    });
    this.http
      .post(
        link,
        {
          ride:ride,
          airportDetail:airportDetail
        },
        {
          headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      )
      .subscribe(
        data => {
          loader.dismiss();
          this.secondStep = true;
          this.firstStep = false;
          
        },
        error => {
          loader.dismiss();
          this.allowCreate = false;
          const toast = this.toastCtrl.create({
              message: 'There was an error creating the ride',
              duration: 3500
            });
            toast.present();
          console.log(error);
        }
      );

  }



}
