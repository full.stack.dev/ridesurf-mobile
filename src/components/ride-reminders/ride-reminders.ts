import { Component } from '@angular/core';
import { NavController, NavParams, Nav, ViewController, ToastController } from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";

import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the RideRemindersComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'ride-reminders',
  templateUrl: 'ride-reminders.html'
})
export class RideRemindersComponent {


  reminders:any;
  empty:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl:ViewController,
  			  public http: HttpClient, public global:GlobalProvider, public toastCtrl: ToastController) {

    this.getReminders();

    console.log(this.reminders);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  getReminders(){
    var link  = this.global.urlApi + "/getRideReminders";

    this.http.get(link,{
          headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           
           if(data['empty'] = 0){
             this.empty = false;

           }else{
             this.empty = true;
             this.reminders = data['reminders'];
           }
      }, error => {
         console.log("Oooops!");
      });
  }

  deleteReminder(id:any, index:any){
  	var link = this.global.urlApi + "/deleteRideReminder";
  	this.http.post(link,
  	{
  		id:id,
  	},
  	{
        headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
        	this.reminders.splice(index, 1);
          const toast = this.toastCtrl.create({
            message: 'Deleted Ride Reminder',
            duration: 3000
          });
          toast.present();
        
      }, error => {
        const toast = this.toastCtrl.create({
          message: 'Error deleting alert, please try again.',
          duration: 3500
        });
        toast.present();
         console.log("Oooops!");
      });
  }
}
