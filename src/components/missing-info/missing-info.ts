import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../../pages/profile/profile';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the MissingInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'missing-info',
  templateUrl: 'missing-info.html'
})
export class MissingInfoComponent {
   	
  //@Input('user') user:any;

  user:any;
  constructor(public navCtrl: NavController, public storage:Storage) {
    console.log('Hello MissingInfoComponent Component');
        this.storage.get('userData').then((val) => {
          this.user = val;
    });
  }

  goToProfile(){
  	this.navCtrl.push(ProfilePage);
  }

}
