import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  LoadingController
} from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { AlertController, ToastController } from "ionic-angular";
import { Platform, ModalController } from "ionic-angular";

import { Observable } from "rxjs/Observable";

import { GlobalProvider } from "../../providers/global/global";
import { CreateMessageComponent } from "../../components/create-message/create-message";
import { UserProvider } from "../../providers/user/user";

/**
 * Generated class for the MessageListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-message-list",
  templateUrl: "message-list.html"
})
export class MessageListPage {

  messages: any;
  url: any;
  user: any;
  isIos:boolean = false;
  interval:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public http: HttpClient,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public app: App,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingController,
    public plt: Platform
  ) {
    var link = this.global.urlApi + "/getAllMessages";
    this.url = this.global.url;
    this.user = this.userProvider.currentUser();
    if(this.plt.is('ios')){
      this.isIos = true;
    }

    this.interval = this.intervalMessage();

    this.getMessages();
  }

  intervalMessage(){
    return Observable.interval(20000).subscribe(() => {
    var link = this.global.urlApi + "/getAllMessages";
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "x-www-form-urlencoded")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log('getM' + data);
          this.messages = data;
        },
        error => {
          console.log(error);

          const toast = this.toastCtrl.create({
            message: "Error getting messages",
            duration: 2000
          });
          toast.present();
        }
      );
     });
  }

  getMessages() {
    let loader = this.loadingCtrl.create({
      content: "Getting message..."
    });
    loader.present();
    var link = this.global.urlApi + "/getAllMessages";
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "x-www-form-urlencoded")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          this.messages = data;
          loader.dismiss();
        },
        error => {
          console.log(error);
          loader.dismiss();
          const toast = this.toastCtrl.create({
            message: "Error getting messages",
            duration: 2000
          });
          toast.present();
        }
      );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MessageListPage");
  }

  ionViewDidLeave(){
    this.interval.unsubscribe();
    clearInterval(this.interval);
  }

  openCreateMessage() {
    let messageModal = this.modalCtrl.create(CreateMessageComponent);
    messageModal.present();
  }

  viewMessages(id: any, before:any) {
    console.log("Click on single message");
    var link = this.global.urlApi + "/getSingleMessage/" + id;

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "x-www-form-urlencoded")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.push("SingleMessagePage", {
            message: data,
            id: id,
            user: this.user,
            before_pay:before,
          });
        },
        error => {
          let prompt = this.alertCtrl.create({
            title: "Error",
            message: error.error.message,
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
        }
      );
  }

  deleteMessage(id: any, index: any) {
    var link = this.global.urlApi + "/deleteSingleMessage/" + id;

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "x-www-form-urlencoded")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          this.messages.splice(this.messages.indexOf(index), 1);
        },
        error => {
          console.log(error);
          let prompt = this.alertCtrl.create({
            title: "Error",
            message: error.error.message,
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
        }
      );
  }
}
