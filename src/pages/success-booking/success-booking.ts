import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RidesBookedPage } from '../rides-booked/rides-booked';

/**
 * Generated class for the SuccessBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-success-booking',
  templateUrl: 'success-booking.html',
})
export class SuccessBookingPage {

  ride:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.ride = this.navParams.get('ride');
  	console.log(this.ride);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessBookingPage');
  }

  pushConduct(){
    this.navCtrl.push('CodeOfConductPage'); 
  }

  rideHistory(){
  	this.navCtrl.setRoot(RidesBookedPage);
  }

}
