import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccessBookingPage } from './success-booking';

@NgModule({
  declarations: [
    SuccessBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(SuccessBookingPage),
  ],
  exports: [SuccessBookingPage],
})
export class SuccessBookingPageModule {}
