import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading} from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  currentUser:any;
  notifications:any;
  actionsCount:any;
  alerts:any;
  tabs:any;
  actions:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public global:GlobalProvider, public http: HttpClient, ) {
   this.alerts = "Alerts"
   this.actions = "Actions";

     var url = this.global.urlApi + '/notificationCounts';
     this.http.get(url,
       {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
           
        },
        error => {

          console.log(error);
          console.log("Oooops!");
        });

      this.tabs = [
          { title: this.actions, root: 'NotificationActionsPage' },
          { title: this.alerts, root: 'NotificationMessagePage' },
       ];
  }

  ionViewDidLoad() {
  	
    console.log('ionViewDidLoad NotificationsPage');
    
  }

  //Tabs
  // actions = 'NotificationActionsPage';
  // messages = 'NotificationMessagePage';
}
