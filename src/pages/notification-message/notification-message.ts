import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController } from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { UserProvider } from "../../providers/user/user";
import { RestProvider } from "../../providers/rest/rest";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController } from "ionic-angular";
import { Events } from "ionic-angular";

/**
 * Generated class for the NotificationMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-notification-message",
  templateUrl: "notification-message.html"
})
export class NotificationMessagePage {
  currentUser: any;
  url: any;
  messages: any;
  isEmpty:boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public userProvider: UserProvider,
    public rest: RestProvider,
    public global: GlobalProvider,
    public loadingController: LoadingController,
    public toastCtrl: ToastController,
    public events:Events
  ) {
    this.currentUser = this.userProvider.currentUser();
    this.url = this.global.url;

    var link = this.global.urlApi + "/getNotificationMessages";
    let loader = this.loadingController.create({
      content: "Loading"
    });
    loader.present();
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.messages = data;
          if(this.messages.length == 0){
            this.isEmpty = true;
          }
          loader.dismiss();
        },
        error => {
          console.log(error);
          console.log("Oooops!");
          loader.dismiss();
        }
      );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NotificationMessagePage");
  }

  viewMessages(id: any) {
    let loader = this.loadingController.create({
      content: "Loading"
    });
    loader.present();
    var link = this.global.urlApi + "/getSingleNotificationMessage/" + id;

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          loader.dismiss();
          this.navCtrl.push("NotificationMessageSinglePage", { message: data });
        },
        error => {
          loader.dismiss();
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  deleteMessage(id: any, index:any) {
    var link = this.global.urlApi + "/deleteSingleMessage/" + id;

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.messages.splice(index,1);
          if(this.messages.length < 1){
            this.isEmpty = true;
          }
          const toast = this.toastCtrl.create({
            message: "Message Deleted",
            duration: 2000
          });
          toast.present();

          this.events.publish("updateMessageCount", 0);
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );

  }
}
