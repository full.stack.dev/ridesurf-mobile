import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationMessagePage } from './notification-message';

@NgModule({
  declarations: [
    NotificationMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationMessagePage),
  ],
  exports:[NotificationMessagePage]
})
export class NotificationMessagePageModule {}
