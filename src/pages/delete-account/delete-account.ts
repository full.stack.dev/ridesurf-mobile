import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import { Nav, Platform } from "ionic-angular";
import { HomePage } from "../home/home";
/**
 * Generated class for the DeleteAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-delete-account",
  templateUrl: "delete-account.html"
})
export class DeleteAccountPage {
  currentUser: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider
  ) {
    this.currentUser = this.userProvider.currentUser();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad DeleteAccountPage");
  }

  deleteAccount() {
    this.userProvider.deleteAccount();
    //this.userProvider.currentUser() = null;
    this.navCtrl.setRoot('BeforeLoginPage');
  }
}
