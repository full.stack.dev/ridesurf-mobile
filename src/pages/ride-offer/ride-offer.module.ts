import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideOfferPage } from './ride-offer';

@NgModule({
  declarations: [
    RideOfferPage,
  ],
  imports: [
    IonicPageModule.forChild(RideOfferPage),
  ],
  exports:[RideOfferPage]
})
export class RideOfferPageModule {}
