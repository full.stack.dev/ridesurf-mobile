import { Component } from "@angular/core";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { IonicPage, NavController, NavParams, App } from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController } from "ionic-angular";

/**
 * Generated class for the RideOfferPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-ride-offer",
  templateUrl: "ride-offer.html"
})
export class RideOfferPage {
  currentUser: any;
  driver: any;
  link: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public global: GlobalProvider,
    public http: HttpClient,
    public app:App,
    public loadingController:LoadingController
  ) {
    this.currentUser = this.userProvider.currentUser();
    if (this.currentUser.driver == true) {
      if (this.currentUser.user.driver[0].status == "Approved") {
        this.driver = true;
      } else {
        this.driver = false;
      }
    } else {
      this.driver = false;
    }
    this.link = this.global.urlApi;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad RideOfferPage");
  }

  oneWay() {
    this.navCtrl.push("OneWayPage");
  }

  roundTrip() {
    this.navCtrl.push("RoundTripPage");
  }
  openRide(){
    this.app.getRootNav().setRoot("OpenRidePage");
  }
  airportRide(){
    let loader = this.loadingController.create({
            content: "Loading"
          });  
        loader.present();

    var link = this.global.urlApi + "/getAirportList";
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
          loader.dismiss();
          this.app.getRootNav().setRoot('AirportRidePage', {airports:res});

        },error => {
          loader.dismiss();
          console.log(error);
          console.log("Oooops!");
        }
      );
    
  }

  driverApply() {
    var link = this.link + "/gettingDriverInfo";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.push("DriverApplyPage", { data: data });
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }
}
