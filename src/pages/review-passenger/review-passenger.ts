import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Events, ToastController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the ReviewPassengerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review-passenger',
  templateUrl: 'review-passenger.html',
})
export class ReviewPassengerPage {

  user_ride:any;
  url:any;
  body:any;
  rating:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
  			  public global:GlobalProvider,public events: Events, 
  			  public toastCtrl: ToastController, public http:HttpClient) {

  	this.user_ride = this.navParams.get('user');

    console.log(this.user_ride);
  	this.url = this.global.url;
  	events.subscribe('star-rating:changed', (starRating) => {
  		this.rating = starRating;
	  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewPassengerPage');
  }

  storeReview(){
    var link = this.global.urlApi + "/reviewPassenger";
    this.http.post(link,{
      userRide_id:this.user_ride.id,
      rating:this.rating,
      body:this.body,
    }, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),

        }).subscribe(data  => {
          const toast = this.toastCtrl.create({
          message: data['message'],
          duration: 3500
        });
        toast.present();
        this.navCtrl.setRoot('PastRidesPage');
           
        }, error => {
         console.log(error);
         console.log("Oooops!");
    });
  }

}
