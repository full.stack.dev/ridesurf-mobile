import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReviewPassengerPage } from './review-passenger';
import { StarRatingModule } from 'ionic3-star-rating';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ReviewPassengerPage,
  ],
  imports: [
  	StarRatingModule,
  	ComponentsModule,
    IonicPageModule.forChild(ReviewPassengerPage),
  ],schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  exports:[ReviewPassengerPage, StarRatingModule]
})
export class ReviewPassengerPageModule {}
