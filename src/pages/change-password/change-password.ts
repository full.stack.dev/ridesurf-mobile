import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController  } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  
  password:any;
  confirmPassword:any;
  reply:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider : UserProvider,
  			  private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  changePassword(){
  	this.reply = this.userProvider.changePassword(this.password, this.confirmPassword);
  	
  }

}
