import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationActionsPage } from './notification-actions';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    NotificationActionsPage,
  ],
  imports: [
  	StarRatingModule,
    IonicPageModule.forChild(NotificationActionsPage),
  ],
  exports:[NotificationActionsPage]
})
export class NotificationActionsPageModule {}
