import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Loading, ModalController } from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { UserProvider } from "../../providers/user/user";
import { RestProvider } from "../../providers/rest/rest";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController, ToastController } from "ionic-angular";
import { CreateMessageComponent } from '../../components/create-message/create-message';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the NotificationActionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-notification-actions",
  templateUrl: "notification-actions.html"
})
export class NotificationActionsPage {
  currentUser: any;
  notifications: any;
  url: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public userProvider: UserProvider,
    public rest: RestProvider,
    public global: GlobalProvider,
    public loadingController: LoadingController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public alertCtrl:AlertController
  ) {
    this.currentUser = this.userProvider.currentUser();
    this.url = this.global.url;

    var link = this.global.urlApi + "/getNotificationActions";
    var messages;
    let loader = this.loadingController.create({
      content: "Loading"
    });
    loader.present();

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.notifications = data;
          loader.dismiss();
        },
        error => {
          console.log(error);
          console.log("Oooops!");
          loader.dismiss();
        }
      );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad NotificationActionsPage");
  }

  ApproveUser(ride_id: any, user_id: any, index:any, id:any) {
    var link = this.global.urlApi + "/acceptPassenger";

    this.http
      .post(
        link,
        {
          ride_id: ride_id,
          user_id: user_id
        },
        {
          headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      )
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.setRoot("SuccessPassengerApprovePage", { data: data });
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  DenyUser(ride_id: any, user_id: any) {
    var link = this.global.urlApi + "/denyPassenger";

    this.http
      .post(
        link,
        {
          ride_id: ride_id,
          user_id: user_id
        },
        {
          headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      )
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.setRoot("SuccessPassengerApprovePage", { data: data });
        },
        error => {

          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  payDriver(ride_id: any, price: any, seats: any) {
    this.navCtrl.push("RidePaymentPage", {
      ride_id: ride_id,
      price: price,
      seats: seats
    });
  }

  reviewDriver(uRide:any, profile:any, name:any){
    this.navCtrl.push('ReviewDriverPage', {id:uRide, profile:profile, name:name});
  }

  showPublicProfile(user:any){
    var array = {
      'user' : user,
    };
    this.navCtrl.push("PublicProfilePage", { 
      user: array,
      same: false,
    });
  }

  ApproveUserOpenRide(ride_id:any, user_id:any, index:any, id:any){
    var link = this.global.urlApi + "/acceptOpenRidePassenger";
    console.log(id);
    this.http
      .post(
        link,
        {
          ride_id: ride_id,
          user_id: user_id,
          user_ride_open_id :id,
        },
        {
          headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      )
      .subscribe(
        data => {
          this.notifications.splice(index, 1);

          this.navCtrl.push("SuccessPassengerApprovePage", { data: data });
        },
        error => {
          const toast = this.toastCtrl.create({
            message: 'There was an error approving user, please try again',
            duration: 3500
          });
          toast.present();
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  DenyUserOpenRide(ride_id: any, user_id: any, index:any) {
    var link = this.global.urlApi + "/denyPassengerOpenRide";

    this.http
      .post(
        link,
        {
          ride_id: ride_id,
          user_id: user_id
        },
        {
          headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      )
      .subscribe(
        data => {
          this.notifications.splice(index, 1);
          const toast = this.toastCtrl.create({
            message: 'Passenger has been denied.',
            duration: 3500
          });
          toast.present();
        },
        error => {
          const toast = this.toastCtrl.create({
            message: 'There was an error denying the passenger, please try again.',
            duration: 3500
          });
          toast.present();
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  contactDriver(user_id:any, rides_id:any){
    var link = this.global.urlApi + "/messageDriver/" + user_id + "/"+ rides_id;

    this.http.get(link, 
    { headers: new HttpHeaders()
            .set("Content-Type", "x-www-form-urlencoded")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer ' + localStorage.getItem('token')),

    }).subscribe(data => {
      if(data != 0){
        this.navCtrl.push('SingleMessagePage', {
          message:data, 
          id: data['id'],
          user:this.currentUser,
          before_pay:data['before_pay']
        });
      }
      else{
        console.log("userID" + user_id);
        let messageModal = this.modalCtrl.create(CreateMessageComponent, {
          otherUser: user_id, 
          ride_id:rides_id,
          rider:true,
        });
        messageModal.present();
      }

    }, error => {
      console.log(error);
      let prompt = this.alertCtrl.create({
            title: 'Error',
            message: error.error.message,
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
    });
  }
  
}
