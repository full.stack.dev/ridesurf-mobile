import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the PaymentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage {
  
  totalPayout:any;
  rides:any;
  currentUser:any;
  noRide:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  			  public http: HttpClient,  public global : GlobalProvider, public user:UserProvider) {

    this.currentUser = this.user.currentUser();
  	this.getRidePayments();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentsPage');
  }


  getRidePayments(){
  	var link = this.global.urlApi + "/getUserPayments";

  	this.http.get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }).subscribe(res  => {
          this.totalPayout = res['weekPayout'];
          this.rides = res['rides'];
          if(this.rides == 0){
            this.noRide = true;
          }
          console.log(res);
      	}, error => {
          
         console.log(error);
         console.log("Oooops!");
    });
  }
}
