import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PaymentSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-success',
  templateUrl: 'payment-success.html',
})
export class PaymentSuccessPage {
  message:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  	this.message = this.navParams.get('data')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentSuccessPage');
  }

  ridesBooked(){
  	this.navCtrl.push('RidesBookedPage');
  }
}
