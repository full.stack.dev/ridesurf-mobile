import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PastBookedPage } from './past-booked';

@NgModule({
  declarations: [
    PastBookedPage,
  ],
  imports: [
    IonicPageModule.forChild(PastBookedPage),
  ],
  exports:[PastBookedPage]
})
export class PastBookedPageModule {}
