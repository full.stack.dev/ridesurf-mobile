import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';
import { CurrentBookedViewPage } from '../current-booked-view/current-booked-view';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the PastBookedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-past-booked',
  templateUrl: 'past-booked.html',
})
export class PastBookedPage {

  rides:any = [];
  url:any;	
  page:any = 0;
  lastPage:any;
  constructor(public navCtrl: NavController, public navParams: NavParams ,
  	public http: HttpClient,  public global : GlobalProvider, public alertCtrl:AlertController) {

  	this.url = this.global.url;
  	this.getPastRides();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PastBookedPage');
  }

  getPastRides(infiniteScroll ?){
  	var link = this.global.urlApi + "/getPastBooked/" + this.page;

  	this.http.get(link, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(res  => {
          if(this.page != res['last_page']){
            this.rides = this.rides.concat(res['data']);
          }
          this.lastPage = res['last_page'];
          if(infiniteScroll){
            infiniteScroll.complete();
          }
      	}, error => {
         console.log(error);
         console.log("Oooops!");
    });
  }

  rateDriver(uRide_id:any, driver_pic:any, driver_name:any){
    this.navCtrl.push('ReviewDriverPage', {id:uRide_id, profile:driver_pic, name:driver_name});
  }

  reportProblem(id:any){
    this.navCtrl.push('ReportProblemPage', {rides_id:id, user_type : 2});
  }

  doInfinite(infiniteScroll) {

      this.page++;
      this.getPastRides(infiniteScroll);
      if(this.page == this.lastPage){
        infiniteScroll.enable(false);
      }

  }

}
