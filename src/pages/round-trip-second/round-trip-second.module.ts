import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoundTripSecondPage } from './round-trip-second';

@NgModule({
  declarations: [
    RoundTripSecondPage,
  ],
  imports: [
    IonicPageModule.forChild(RoundTripSecondPage),
  ],
  exports:[RoundTripSecondPage]
})
export class RoundTripSecondPageModule {}
