import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Slides } from "ionic-angular";
import { FormBuilder, FormGroup } from "@angular/forms";
import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController, ToastController } from "ionic-angular";
import {
	HttpClient,
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpHeaders
} from "@angular/common/http";
import { AlertController } from "ionic-angular";

/**
 * Generated class for the RoundTripSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
			
@IonicPage()
@Component({
	selector: "page-round-trip-second",
	templateUrl: "round-trip-second.html"
})
export class RoundTripSecondPage {
 	 @ViewChild(Slides) slides: Slides;

	  rideForm: FormGroup;
	  currentUser: any;
	  distance: any;
	  duration: any;
	  pricePerSeat: any;
	  date: String = new Date().toISOString();
	  ride: any;
	  repeat: boolean = false;
	  location_from: any;
	  location_to: any;
	  minDate: string = new Date().toISOString();
	  maxDate: string = new Date("12/31/2050").toISOString();
	  waypoints: any;
	  isDisabled: boolean = true;
	  ladies_only: boolean = false;
	  purposes:any = [];
	  luggageAmount:any=1;
	  seats:any=1;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private formBuilder: FormBuilder,
		public http: HttpClient,
		public userProvider: UserProvider,
		public global: GlobalProvider,
		public loadingController: LoadingController,
		public alertCtrl: AlertController,
		public toastCtrl: ToastController
	) {
		this.ride = this.navParams.get("ride");
	    this.repeat = this.navParams.get("repeat");
	    this.distance = navParams.get("distance");
	    this.location_from = this.navParams.get("location_from");
	    this.location_to = this.navParams.get("location_to");
	    this.waypoints = this.navParams.get("waypoints");
	    this.pricePerSeat = this.navParams.get('price');
	    // this.minDate = new Date().toISOString();

	    this.currentUser = this.userProvider.currentUser();
	    this.getPurposes();

	    if (this.repeat == false) {
	      this.rideForm = this.formBuilder.group({
	        dateBeginning: [""],
			dateReturn: [""],
			timeStart:[""],
			timeReturn:[""],
	        seats: [""],
	        price: [this.pricePerSeat],
	        luggageSize: [""],
	        luggageAmount: [""],
	        description: [""],
	        womenOnly: [this.ladies_only],
	        trip_purpose_id:[""]
	      });
	    }
	  }

	  ionViewDidLoad() {
	    console.log("ionViewDidLoad OneWaySecondPage");
	  }

	  changeLadies($event) {
	    if (this.ladies_only == false) {
	      this.ladies_only = true;
	    } else {
	      this.ladies_only = false;
	    }
	  }

	  nextSlide() {
	    this.slides.slideNext();
	  }

	  allowContinue() {
	  	if(this.rideForm.value.dateBeginning != "" && this.rideForm.value.dateReturn != "" 
	  		&& this.rideForm.value.timeStart != ""){
	    		this.isDisabled = false;
	    	}
	  }

	  uploadRide() {
	    if (this.ladies_only == true) {
	      var ladies = "Yes";
	    } else {
	      var ladies = "No";
	    }
	    let loader = this.loadingController.create({
	      content: "Loading"
	    });
	    loader.present();

	    if(this.rideForm.value.dateReturn == ""){
	    	loader.dismiss();
	    	const toast = this.toastCtrl.create({
                  message: 'Return date is empty',
                  duration: 3000,
            });
            toast.present();
            return false;
            
	    }
	    if(this.rideForm.value.dateBeginning == ""){
	    	loader.dismiss();
	    	const toast = this.toastCtrl.create({
                  message: 'Outbound date is empty',
                  duration: 3000,
            });
            toast.present();

            return false;
	    }
	    if(this.rideForm.value.dateBeginning == this.rideForm.value.dateReturn){
	    	loader.dismiss();
	    	const toast = this.toastCtrl.create({
                  message: 'Your return day cannot be the same as Outbound',
                  duration: 3000,
            });
            toast.present();

            return false;
	    }
	    if(this.rideForm.value.description == "" ){
	    	loader.dismiss();
	    	const toast = this.toastCtrl.create({
                  message: 'Please fill out a Description',
                  duration: 3000,
            });
            toast.present();

            return false;
	    }
	    if(this.rideForm.value.trip_purpose_id == ""){
	    	this.rideForm.value.trip_purpose_id = 1;
	    }
	    if(this.rideForm.value.luggageSize == ""){
	    	this.rideForm.value.luggageSize = "Small";
	    }

	    let Form = JSON.stringify(this.rideForm.value);
	    console.log(this.pricePerSeat);

	    var link = this.global.urlApi + "/createRideRountrip";
	    var data = {
	      dateBeginning: this.rideForm.value.dateBeginning,
	      dateReturn: this.rideForm.value.dateReturn,
	      timeStart: this.rideForm.value.timeStart,
	      timeReturn: this.rideForm.value.timeReturn,
	      seats: this.seats,
	      price: this.pricePerSeat,
	      luggage_size: this.rideForm.value.luggageSize,
	      luggage_amount: this.luggageAmount,
	      startHour: this.rideForm.value.time,
	      description: this.rideForm.value.description,
	      ladies_only: ladies,
	      location_from: this.location_from,
	      location_to: this.location_to,
	      duration: this.navParams.get("duration"),
	      seats_available: this.seats,
	      trip_purpose_id:this.rideForm.value.trip_purpose_id,
	      waypoints: JSON.stringify(this.waypoints)
	    };
	    this.http
	      .post(
	        link,
	        {
	          data: data
	        },
	        {
	          headers: new HttpHeaders()
	            .set("Accept", "application/json")
	            .set("Authorization", "Bearer " + localStorage.getItem("token")),
	          observe: "body"
	        }
	      )
	      .subscribe(
	        data => {
	          console.log(data);
	          this.navCtrl.setRoot("SuccessCreateRidePage", { data: data["message"],type:0, });
	          loader.dismiss();
	        },
	        error => {
	          loader.dismiss();
	          let prompt = this.alertCtrl.create({
	            title: "Error",
	            message: "Error " + error.error.message,
	            buttons: [
	              {
	                text: "Close",
	                handler: data => {}
	              }
	            ]
	          });
	          prompt.present();
	          console.log(error);
	        }
	      );
	  }

	  getPurposes(){
	    var link = this.global.urlApi + "/fetchPurposes";
	    this.http.get(link, {
	        headers: new HttpHeaders()
	          .set("Content-Type", "Application/json")
	          .set("Accept", "application/json")
	          .set("Authorization", "Bearer " + localStorage.getItem("token"))
	      })
	      .subscribe(res => {
	         console.log(res);
	         this.purposes = res;
	       },error => {
	          console.log(error);
	          console.log("Oooops!");
	        }
	      );
	  }

	  addLuggage(){
	    if(this.luggageAmount < 6)
	      this.luggageAmount++;
	  }

	  subLuggage(){
	    if(this.luggageAmount > 0)
	      this.luggageAmount--;
	  }

	  addSeats(){
	     if(this.seats < 5){
	       this.seats++;
	     }
	  }

	  subSeats(){
	    if(this.seats > 1){
	      this.seats--;
	    }
	  }
}
