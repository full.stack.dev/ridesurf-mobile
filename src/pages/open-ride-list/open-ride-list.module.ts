import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenRideListPage } from './open-ride-list';

@NgModule({
  declarations: [
    OpenRideListPage,
  ],
  imports: [
    IonicPageModule.forChild(OpenRideListPage),
  ],
  exports:[OpenRideListPage],
})
export class OpenRideListPageModule {}
