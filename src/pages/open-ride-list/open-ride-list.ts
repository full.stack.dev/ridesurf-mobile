import { Component } from '@angular/core';
import { App } from 'ionic-angular';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';

import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';
/**
 * Generated class for the OpenRideListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-open-ride-list',
  templateUrl: 'open-ride-list.html',
})
export class OpenRideListPage {

  rides:any = [];
  currentUser:any;
  page:any = 1;
  lastPage:any;
  driver:any;
  indexStart:any;
  lastIndex:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public userProvider: UserProvider,
    		  public http: HttpClient,  public global : GlobalProvider, public app:App, public toastCtrl: ToastController,
          public alertCtrl: AlertController) {

  	this.getCurrentOpenRides();
  	this.currentUser = this.userProvider.currentUser();
    this.driver = this.currentUser.driver;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenRideListPage');
  }

  getCurrentOpenRides(infiniteScroll ?){

  	var link = this.global.urlApi + "/getOpenRides/" + this.page;
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
          if (res["data"] != null ) {
            if(this.page != res['last_page'] + 1 ){
              this.rides = this.rides.concat(res["data"]);
            }
            
            this.lastPage = res["last_page"];
            if (infiniteScroll) {
              infiniteScroll.complete();
            }
          }
          console.log(this.rides);
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );

  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.getCurrentOpenRides(infiniteScroll);
    if (this.page == this.lastPage) {
      infiniteScroll.enable(false);
    }
  }


  editRide(ride){
  	this.navCtrl.push('EditOpenRidePage', {openRide: ride});
  }

  changeStatus(id){
  	var link = this.global.urlApi + "/changeStatus/" + id;
  	this.http
      .post(link, null,{
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
        	const toast = this.toastCtrl.create({
              message: res['message'],
              duration: 3500
            });
            toast.present();
           console.log(this.rides);
        },
        error => {
	    	const toast = this.toastCtrl.create({
	          message: 'Server Error please try again',
	          duration: 3500
	        });
	        toast.present();
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  deleteRide(id:any, index:any){

    let alert = this.alertCtrl.create({
        title: 'Confirm delete ride',
        message: 'Are you sure you want to permanently delete this ride?',
        buttons: [
            {
                text: 'No',
                handler: () => {
                    console.log('Cancel clicked');
                }
            },
            {
                text: 'Yes',
                handler: () => {
                   this.confirmDelete(id,index);
                }
            }
        ]
    })

  	alert.present();
  }

  confirmDelete(id:any, index:any){
    var link = this.global.urlApi + "/deleteOpenRide/" + id;
    this.http
      .post(link, null,{
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
          console.log(index);

          this.rides.splice(index, 1);
          const toast = this.toastCtrl.create({
              message: res['message'],
              duration: 3500
            });
            toast.present();
             
        },
        error => {
        const toast = this.toastCtrl.create({
            message: 'Server Error please try again',
            duration: 3500
          });
          toast.present();
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

}
