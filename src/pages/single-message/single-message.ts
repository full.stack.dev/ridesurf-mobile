import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { UserProvider } from '../../providers/user/user';
import { interval } from 'rxjs/observable/interval';


/**
 * Generated class for the SingleMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-single-message',
  templateUrl: 'single-message.html',
})
export class SingleMessagePage {
  @ViewChild(Content) content: Content;

  messages:any;
  messageform:any;
  url:any
  thread:any;
  currentUser:any;
  isIOS:boolean = false
  messageLoader:any;
  dimensions:any;
  profileImg:any;
  before_pay:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
              public global : GlobalProvider, public http: HttpClient, public alertCtrl:AlertController,  
              public userProvider : UserProvider, public plt: Platform) {

    this.url = this.global.url;
    this.messages = navParams.get('message');
    this.thread = navParams.get('id');
    this.currentUser = this.navParams.get('user');
    this.before_pay = this.navParams.get('before_pay');
    this.profileImg = this.messages.reciver_img;

    console.log(this.profileImg);
    console.log(this.before_pay);
    if(this.plt.is('ios')){
      this.isIOS = true;
    }


    this.messageLoader = this.getMessages();
    setTimeout(() => {

        this.content.scrollToBottom(200);
     }, 300);
  }

  scrollDown(){
    setTimeout(() => {
        this.content.scrollToBottom(200);
     }, 300);
  }

  getMessages(){
    return Observable.interval(10000).subscribe(() => {
        var link = this.global.urlApi + "/getSingleMessage/" + this.thread;
        
        this.http.get(link, 
        { headers: new HttpHeaders()
                .set("Content-Type", "x-www-form-urlencoded")
                .set("Accept", "application/json")
                .set('Authorization', 'Bearer ' + localStorage.getItem('token')),

        }).subscribe(data => {
          
           this.messages = data;
           this.scrollDown();
        }, error => {
          console.log(error);
        });
    });
  }

  ionViewDidLoad() {
    
    console.log('We enter single message');
  }

  ionViewWillLeave() {
    this.messageLoader.unsubscribe();
  }

  sendMessage(reciver:any){
    
    var id = this.messages.id;
    var body = this.messageform;
    var link = this.global.urlApi + "/sendMessage/" + id + "/" +reciver;
    console.log(this.before_pay);
    if(this.before_pay){
      var phoneChange = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/;
      var emailChange =  /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;

      body = body.replace(phoneChange, '########');
      body = body.replace(emailChange, '*********');
    }

    this.messageform = null;


    this.http.post(link, {
      body:body,
    },
    { headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer ' + localStorage.getItem('token')),

    }).subscribe(data => {
      this.messages = data;
      this.scrollDown();
    }, error => {
      console.log(error);
      let prompt = this.alertCtrl.create({
            title: 'Error',
            message: error.error.message,
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
    });

  }



}
