import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleMessagePage } from './single-message';

@NgModule({
  declarations: [
    SingleMessagePage,
  ],
  imports: [
    IonicPageModule.forChild(SingleMessagePage),
  ],
  exports:[SingleMessagePage]
})
export class SingleMessagePageModule {}
