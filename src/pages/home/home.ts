import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	currentUser:any;
	temp:any;
	constructor(public navCtrl: NavController, public userProvider: UserProvider,
				public storage : Storage) {
		
		this.storage.get('userData').then((val) => {

			if(val != null){
	        	this.currentUser = val;
	       	}else{
	       		this.navCtrl.setRoot('BeforeLoginPage');
	       	}
	    });
	  	
	}

  	rideList(){
	   	this.navCtrl.push('RideSearchPage');
	}

	rideOffer(){
		this.navCtrl.push('RideOfferPage');
	}
}
