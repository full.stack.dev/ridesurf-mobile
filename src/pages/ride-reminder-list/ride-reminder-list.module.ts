import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideReminderListPage } from './ride-reminder-list';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    RideReminderListPage,
  ],
  imports: [
    IonicPageModule.forChild(RideReminderListPage),
    ComponentsModule,
  ],
  exports:[RideReminderListPage],
})
export class RideReminderListPageModule {}
