import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { HomePage } from "../home/home";
/**
 * Generated class for the DeleteRidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-delete-ride',
  templateUrl: 'delete-ride.html',
})
export class DeleteRidePage {
	
  ride:any;
  bodyMessage:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
            public http: HttpClient,  public global : GlobalProvider, public toastCtrl: ToastController) {

  	this.ride = this.navParams.get('data');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeleteRidePage');
  }

  //Delete ride from driver
  cancelRide(){
    var link = this.global.urlApi + "/deleteRide";
    let loader = this.loadingCtrl.create({
      content: "Deleting..."
    });

    loader.present();
    this.http.post(link,{
      ride:this.ride,
      bodyMessage:this.bodyMessage,
    }, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data  => {
           loader.dismiss();

           const toast = this.toastCtrl.create({
              message: data['message'],
              duration: 3500
            });
            toast.present();
            this.navCtrl.setRoot( 'CurrentRidesPage' );

        }, error => {

         loader.dismiss();
         const toast = this.toastCtrl.create({
              message: 'There was an error deleting your ride',
              duration: 3500
            });
            toast.present();
         console.log(error);
         console.log("Oooops!");
    });
  }

}
