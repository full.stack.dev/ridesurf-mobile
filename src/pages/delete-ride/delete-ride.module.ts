import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteRidePage } from './delete-ride';

@NgModule({
  declarations: [
    DeleteRidePage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteRidePage),
  ],
  exports:[DeleteRidePage]
})
export class DeleteRidePageModule {}
