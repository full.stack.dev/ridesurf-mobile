import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrentBookedViewPage } from './current-booked-view';

@NgModule({
  declarations: [
    CurrentBookedViewPage,
  ],
  imports: [
    IonicPageModule.forChild(CurrentBookedViewPage),
  ],
  exports:[CurrentBookedViewPage]
})
export class CurrentBookedViewPageModule {}
