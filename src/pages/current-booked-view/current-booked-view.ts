import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the CurrentBookedViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-current-booked-view',
  templateUrl: 'current-booked-view.html',
})
export class CurrentBookedViewPage {
  
  ride:any;
  duration:any;
  url:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  			  public global:GlobalProvider) {
  	
  	this.ride = this.navParams.get('ride');
  	this.url = this.global.url;

  	var date = new Date(null);
	date.setSeconds(this.ride.ride.duration); // specify value for SECONDS here
	this.duration = date.toISOString().substr(11, 8);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurrentBookedViewPage');
  }

}
