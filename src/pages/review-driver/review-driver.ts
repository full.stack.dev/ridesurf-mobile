import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';
import { Events, ToastController } from 'ionic-angular';
import { RidesBookedPage } from '../rides-booked/rides-booked';

/**
 * Generated class for the ReviewDriverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review-driver',
  templateUrl: 'review-driver.html',
})
export class ReviewDriverPage {
  
  rating:number;
  body:string;
  uRide_id:any;
  profile:any;
  name:any;
  url:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  			 public http: HttpClient,  public global : GlobalProvider,
  			 public events: Events, public toastCtrl: ToastController) {
    this.profile = this.navParams.get('profile');
    this.name = this.navParams.get('name');
    
  	this.uRide_id = this.navParams.get('id');
    this.url = this.global.url;
  	events.subscribe('star-rating:changed', (starRating) => {
  		this.rating = starRating;
	  });
    
    console.log(this.profile);
    console.log(this.name);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewDriverPage');
  }

  storeReview(){
    if(this.rating == null){
      const toast = this.toastCtrl.create({
          message: 'Please enter a star rating.',
          duration: 3500
        });
        toast.present();

        return false;
    }
  	var link = this.global.urlApi + "/reviewDriver";
  	this.http.post(link,{
  		userRide_id:this.uRide_id,
  		rating:this.rating,
  		body:this.body,
  	}, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),

        }).subscribe(data  => {
           const toast = this.toastCtrl.create({
		      message: data['message'],
		      duration: 3500
		    });
		    toast.present();
           this.navCtrl.push( RidesBookedPage );
           
      	}, error => {
         console.log(error);
         console.log("Oooops!");
    });
  }



}
