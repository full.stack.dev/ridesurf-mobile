import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReviewDriverPage } from './review-driver';
import { StarRatingModule } from 'ionic3-star-rating';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ReviewDriverPage,
  ],
  imports: [
  	StarRatingModule,
  	ComponentsModule,
    IonicPageModule.forChild(ReviewDriverPage),
  ],schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  exports:[ReviewDriverPage, StarRatingModule]
})
export class ReviewDriverPageModule {}
