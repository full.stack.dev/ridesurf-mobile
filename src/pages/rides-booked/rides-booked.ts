import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the RidesBookedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rides-booked',
  templateUrl: 'rides-booked.html',
})
export class RidesBookedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RidesBookedPage');
  }

  pastBooked = 'PastBookedPage';
  currentBooked = 'CurrentBookedPage';
}
