import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    PasswordStrengthMeterModule,
    IonicPageModule.forChild(RegisterPage),
  ],
  exports:[RegisterPage]
})
export class RegisterPageModule {}
