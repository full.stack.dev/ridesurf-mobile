import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl  } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GlobalProvider } from '../../providers/global/global';
import { Events, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { PasswordValidator } from '../../validators/password.validator';
import { LoadingController } from "ionic-angular";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registrationForm: FormGroup;
  public conduct:boolean = false;
  checked:boolean;
  password:any;
  confirm_password:any;
  currentUser:any;
  show:boolean = false;
  doNotMatch:boolean = false;
  strengthChange = 0;
  control:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global:GlobalProvider,
    private formBuilder: FormBuilder, private alertCtrl: AlertController, public userProvider: UserProvider,
    public fb: Facebook, public events: Events, public storage : Storage, public toastCtrl:ToastController,
    public http: HttpClient, public loadingController: LoadingController) {

    this.control = new FormControl('', [Validators.required, 
                   Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,30}')]);

    this.registrationForm = this.formBuilder.group({
      firstname:['', Validators.required],
      lastname:['', Validators.required],    
      email: ['', Validators.required],
      password: ['', [ 
                Validators.required,
                Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,30}') ] 
              ],
      confirm_password:['', Validators.required],
      conduct: this.checked,


    });

  }

   loginFacebook(){
 
    localStorage.clear();
    
    var facebook_user;
    // Login with permissions
    this.fb.login(['public_profile', 'email'])
    .then( (res: FacebookLoginResponse) => {

        // The connection was successful
        if(res.status == "connected") {
            // Get user ID and Token
            var fb_id = res.authResponse.userID;
            var fb_token = res.authResponse.accessToken;
            // Get user infos from the API
            this.fb.api("/me?fields=id,first_name,last_name,picture.type(large),gender,email", []).then((user) => {
                facebook_user = user;
                // Get the connected user details
                console.log('user info :');
                console.log(facebook_user);

               var link = this.global.urlApi + "/loginFacebook";

               this.http.post(link, {
                 first_name:user.first_name,
                 last_name:user.last_name,
                 id:user,
                 picture:user.picture,
               },
               { headers: new HttpHeaders()
                  .set('Accept', 'application/json ')

              }).subscribe(data => {

                console.log(data);
                if(data['action'] == 1){
                  this.currentUser = data;
                  this.storage.set('userData', data);
                  this.events.publish('user:login', data);
                  localStorage.setItem('token' , data['token']);
                  localStorage.setItem('userData' , JSON.stringify(data));
                  window["plugins"].OneSignal.sendTag("userid",data['user_id'])
                }else{
                  const toast = this.toastCtrl.create({
                    message: "Error trying to access Facebook",
                    duration: 3500
                  });

                   toast.present();
                }

              }, error => {
                 const toast = this.toastCtrl.create({
                    message: "There was an error connecting to our servers",
                    duration: 3500
                  });
                 toast.present();
                console.log(error);
              });

                // => Open user session and redirect to the next page

            });

        } 
        // An error occurred while loging-in
        else {
            const toast = this.toastCtrl.create({
                message: "There was a problem logging into your account, if the problem persists please contact us",
                duration: 3500
              });
             toast.present();
            console.log("An error occurred...");

        }

    })
    .catch((e) => {
          const toast = this.toastCtrl.create({
              message: "There was a problem logging into account, if the problem persists please contact us",
              duration: 3500
            });
          toast.present();
        console.log('Error logging into Facebook', e);
    });

  }

  pushToTerms(){
    this.navCtrl.push('TermsPage');
  }

  pushConduct(){
    this.navCtrl.push('CodeOfConductPage'); 
  }
  pushPrivacy(){
    this.navCtrl.push('PrivacyPage');
  }

  matching(){
    console.log(this.strengthChange);
    setTimeout(function(){
      if(this.password !== this.confirm_password){
        this.doNotMatch = false;
      }else{
        this.doNotMatch = true;
      }
    },500);

  }

  acceptTerms(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  login(){
    this.navCtrl.push('LoginPage');
  }

  sendRegistration(){

    let loader = this.loadingController.create({
        content: "Loading"
      });  
    loader.present().then(()=>{
      
      //Check that the passwords matches
      if(this.password != this.confirm_password){
          loader.dismissAll();
          let alert = this.alertCtrl.create({
          title: '',
          message: 'Passwords do not match',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
            }
          ]
        });
        alert.present();
        return false;
       }
       else if(this.registrationForm.value.firstname == null){
         loader.dismissAll();
          let alert = this.alertCtrl.create({
          title: 'Please correct the following field',
          message: 'First Name is empty',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel'
            }
          ]
        });
        alert.present();
        return false;
       }
       else if(this.registrationForm.value.email == null){
         loader.dismissAll();
          let alert = this.alertCtrl.create({
          title: 'Please correct the following field',
          message: 'Enter a valid email',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel'
            }
          ]
        });
        alert.present();
        return false;
       }
       else if(this.registrationForm.value.lastname == null){
         loader.dismissAll();
          let alert = this.alertCtrl.create({
          title: 'Please correct the following field',
          message: 'Last Name is empty',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
            }
          ]
        });
        alert.present();
        return false;
       }else if(this.control.invalid){
         loader.dismissAll();
          let alert = this.alertCtrl.create({
          title: 'Password is not secure enough',
          message: 'Minimum of 8 characters. Use at least one number, uppercase & lowercase letter and symbol.',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
            }
          ]
        });
        alert.present();

       }else if(!this.registrationForm.value.email.includes('@')){
         loader.dismissAll();
          let alert = this.alertCtrl.create({
          title: 'Invalid Email',
          message: 'Please make sure you enter a valid email',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
            }
          ]
        });
        alert.present();
        return false;
       }
       else{
        let self = this;

        //send data to the server
        var data = {
          'first_name' : this.registrationForm.value.firstname,
          'last_name' : this.registrationForm.value.lastname,
          'email' : this.registrationForm.value.email,
          'password' : this.password,
        }

        var link = this.global.urlApi + "/registerUser";
        var headers = new Headers();
        headers.append('Accept', 'application/json');
        this.http.post(link,{
          "Headers": headers,
          "data" : data,
        }).subscribe(data => {
          loader.dismissAll();
          if(data['action'] == 1){
              localStorage.clear();
              const toast = this.toastCtrl.create({
                message: "Succesfully registered account, please verify your email.",
                duration: 3500
              });
              toast.present();
              this.storage.set('userData', data);
              this.events.publish('user:login', data);
              localStorage.setItem('token' , data['token']);
              localStorage.setItem('userData' , JSON.stringify(data));
              // window["plugins"].OneSignal.sendTag("userid",data['user_id'])
             
          }else{
            loader.dismissAll();
            let alert = this.alertCtrl.create({
              title: '',
              message: data['message'],
              buttons: [
                {
                  text: 'Ok',
                  role: 'cancel',
                }
              ]
            });
            alert.present();

          }
 
        }, error => {
          loader.dismissAll();
          let alert = this.alertCtrl.create({
            title: 'There was a server error trying to register',
            message: error ,
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
              }
            ]
          });
          alert.present();
        });
        
      }
    });  

  }

}