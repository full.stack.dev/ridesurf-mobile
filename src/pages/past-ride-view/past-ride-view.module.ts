import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PastRideViewPage } from './past-ride-view';

@NgModule({
  declarations: [
    PastRideViewPage,
  ],
  imports: [
    IonicPageModule.forChild(PastRideViewPage),
  ],
  exports:[PastRideViewPage]
})
export class PastRideViewPageModule {}
