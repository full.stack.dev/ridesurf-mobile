import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the PastRideViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-past-ride-view',
  templateUrl: 'past-ride-view.html',
})
export class PastRideViewPage {

  ride:any;
  duration:any;
  time:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global:GlobalProvider,
              public loadingCtrl: LoadingController, public http: HttpClient, public toastCtrl: ToastController) {

    this.ride = this.navParams.get('ride');
    console.log(this.ride);
    var date = new Date(null);
    date.setSeconds(this.ride.duration); // specify value for SECONDS here
    this.duration = date.toISOString().substr(11, 8);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PastRideViewPage');
  }

  repeatRide(ride:any){

    var link = this.global.urlApi + "/repeatRide";

    let loader = this.loadingCtrl.create({
      content: "Creating new Ride ..."
    });

    loader.present();
    this.http.post(link,{
      ride: this.ride,
     
    }, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data  => {
           loader.dismiss();
           this.navCtrl.push('SuccessCreateRidePage', {data:data});

        }, error => {
          loader.dismiss();
          const toast = this.toastCtrl.create({
              message: 'There was an error trying to repat the ride please try again another time.',
              duration: 4000
          });
          toast.present();
          console.log(error);
          console.log("Oooops!");
    });
  }
}
