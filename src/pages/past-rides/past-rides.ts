import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, ToastController } from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { ModalController } from "ionic-angular";

import { GlobalProvider } from "../../providers/global/global";
import { UserProvider } from "../../providers/user/user";

import { PassengerReviewsComponent } from "../../components/passenger-reviews/passenger-reviews";

/**
 * Generated class for the PastRidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-past-rides",
  templateUrl: "past-rides.html"
})
export class PastRidesPage {
  pastRides: any = [];
  currentUser: any;
  page: any = 0;
  lastPage: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    public http: HttpClient,
    public global: GlobalProvider,
    public userProvider: UserProvider,
    public toastCtrl: ToastController
  ) {
    this.getPastRides();
    this.currentUser = this.userProvider.currentUser();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PastRidesPage");
  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.getPastRides(infiniteScroll);
    if (this.page == this.lastPage) {
      infiniteScroll.enable(false);
    }
  }

  getPastRides(infiniteScroll?) {
    var link = this.global.urlApi + "/getPastRides/" + this.page;
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
          if (res["data"] != null && res["to"] != null) {
            if(this.page != res['last_page']){
              this.pastRides = this.pastRides.concat(res["data"]);
            }
            console.log(res);
            this.lastPage = res["last_page"];
            if (infiniteScroll) {
              infiniteScroll.complete();
            }
          }
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  viewRide(ride: any) {
    this.navCtrl.push("PastRideViewPage", { ride: ride });
  }

  driverApply() {
    var link = this.global.urlApi + "/gettingDriverInfo";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.push("DriverApplyPage", { data: data });
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  reportProblem(id: any, passenger:any) {
    if(passenger > 0)
      this.navCtrl.push("ReportProblemPage", { rides_id: id, user_type : 1 });
    else{
      const toast = this.toastCtrl.create({
        message: 'There are no passengers in this ride',
        duration: 4000
      });
      toast.present();
    }
            
  }

  completeRide(ride: any) {
    let messageModal = this.modal.create(PassengerReviewsComponent, {
      user_rides: ride.user_ride
    });
    messageModal.present();
  }
}
