import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

/**
 * Generated class for the BeforeLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-before-login',
  templateUrl: 'before-login.html',
})
export class BeforeLoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public userProvider: UserProvider,
              public storage : Storage) {

        this.storage.get('userData').then((val) => {
          
          if(val != null){
            
            this.navCtrl.setRoot(HomePage);
          }

      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BeforeLoginPage');
  }

  signIn(){
  	this.navCtrl.push('LoginPage');
  }

  register(){
  	this.navCtrl.push('RegisterPage');
  }
}
