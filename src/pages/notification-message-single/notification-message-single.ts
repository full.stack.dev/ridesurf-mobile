import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from "../../providers/rest/rest";
import { Events } from "ionic-angular";

/**
 * Generated class for the NotificationMessageSinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification-message-single',
  templateUrl: 'notification-message-single.html',
})
export class NotificationMessageSinglePage {
  url:any;	
  message:any;
  count:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public rest:RestProvider, 
  			  public global:GlobalProvider, public events:Events) {
  	this.message = this.navParams.get('message');
  	this.url = this.global.url;
    this.rest.getNotifications(count => {
      this.count = count;
    });
    this.events.publish("updateMessageCount", this.count);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationMessageSinglePage');
  }

}
