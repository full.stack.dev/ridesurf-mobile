import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationMessageSinglePage } from './notification-message-single';

@NgModule({
  declarations: [
    NotificationMessageSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationMessageSinglePage),
  ],
  exports:[NotificationMessageSinglePage]
})
export class NotificationMessageSinglePageModule {}
