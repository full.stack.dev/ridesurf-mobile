import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the PublicProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-public-profile',
  templateUrl: 'public-profile.html',
})
export class PublicProfilePage {

  url:any;
  currentUser:any;
  preferences:any;
  rating:any;
  sameProfile:boolean = false; 

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient,

  	public global : GlobalProvider) {

  		this.url = this.global.url;
  		this.currentUser = this.navParams.get('user');
      this.rating = this.currentUser.user.rating;
      this.sameProfile = this.navParams.get('same');


      if(this.sameProfile == true){
        this.preferences = this.navParams.get('preferences');
      }else{
        this.currentUser.avatar = this.currentUser.user.avatar;
        if( this.currentUser.user.preferences != null){
          this.preferences = this.currentUser.user.preferences;
        }else{
          this.getUserPreferences(this.currentUser.user.id);
        }
      }
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PublicProfilePage');
  }

  viewReview(){
    this.navCtrl.push('UserReviewsPage', {user: this.currentUser});
  }

  getUserPreferences(id:any){
    var link = this.global.urlApi + "/getOtherUserPreferences/" + id;

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.preferences = data;
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

}
