import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicProfilePage } from './public-profile';
import { StarRatingModule } from 'ionic3-star-rating';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PublicProfilePage,
  ],
  imports: [
  	ComponentsModule,
  	StarRatingModule,
    IonicPageModule.forChild(PublicProfilePage),
  ],
  exports:[PublicProfilePage]
})
export class PublicProfilePageModule {}
