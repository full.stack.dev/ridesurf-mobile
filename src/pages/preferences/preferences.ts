import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController} from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the PreferencesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preferences',
  templateUrl: 'preferences.html',
})
export class PreferencesPage {

  preferences:any;
  preferenceForm:FormGroup;
  options:any;
  reply:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	private formBuilder: FormBuilder, public userProvider: UserProvider,
  	public loadingCtrl: LoadingController) {

  	this.preferences = this.navParams.get('preferences');
  	this.options = this.navParams.get('options');

  	let group = {};
  	for(var i=0; i < this.preferences.length; i++){
  		group[this.preferences[i].preference.category] = [this.preferences[i].preference.description];
  	}

  	this.preferenceForm = this.formBuilder.group(group);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferencesPage');
  }

  updatePreferences(){
  	var data = this.preferenceForm.value;
  	let loader = this.loadingCtrl.create({
	    content: "Uploading..."
	  });

	loader.present();
  	this.userProvider.updatePreferences(data);
  	loader.dismiss();
  }
  
}
