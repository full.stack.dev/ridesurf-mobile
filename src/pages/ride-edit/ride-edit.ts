import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Nav } from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { AlertController } from "ionic-angular";
import { LoadingController } from "ionic-angular";
import { Storage } from "@ionic/storage";

/**
 * Generated class for the RideEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-ride-edit",
  templateUrl: "ride-edit.html"
})
export class RideEditPage {
  dateDisabled: boolean = false;
  currentUser: any;
  ride: any;
  rideForm: FormGroup;
  women_only: boolean = false;
  stops:any = [];
  isStop:boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public http: HttpClient,
    public userProvider: UserProvider,
    public global: GlobalProvider,
    public loadingController: LoadingController,
    public alertCtrl: AlertController,
    public nav: Nav,
    public storage: Storage
  ) {
    this.currentUser = this.userProvider.currentUser();
    this.ride = this.navParams.get("data");

    if (this.ride.passengers != null) {
      this.dateDisabled = true;
    }

    if(this.ride.stop){
      var temp = this.ride.stop;
      for(var i = 0; i < temp.length; i++){
        this.stops[i] = {
          'id': temp[i].id,
          'city_name' : temp[i].city_name,
          'price_from' : temp[i].price_from,
          'price_to': temp[i].price_to,
        }
      }
      this.isStop = true;
    }

    console.log(this.stops);

    this.rideForm = this.formBuilder.group({
      date: this.ride.dateFrom,
      seats: this.ride.seats,
      price: this.ride.price,
      luggageSize: this.ride.luggage_size,
      luggageAmount: this.ride.luggage_amount,
      time: this.ride.startHour,
      description: this.ride.description,
      womenOnly: this.ride.ladies_only
    });


  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad RideEditPage");
  }

  pop() {
    this.navCtrl.pop();
  }

  editRide() {
    let loader = this.loadingController.create({
      content: "Loading"
    });
    loader.present();

    let Form = JSON.stringify(this.rideForm.value);

    var link = this.global.urlApi + "/updateRide/" + this.ride.id;

    var data = {
      dateFrom: this.rideForm.value.date,
      seats: this.rideForm.value.seats,
      price: this.rideForm.value.price,
      luggage_size: this.rideForm.value.luggageSize,
      luggage_amount: this.rideForm.value.luggageAmount,
      startHour: this.rideForm.value.time,
      description: this.rideForm.value.description,
      ladies_only: this.rideForm.value.womenOnly,
      seats_available: this.rideForm.value.seats,
      stops : this.stops,
    };

    this.http
      .post(
        link,
        {
          data: data
        },
        {
          headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token")),
          observe: "body"
        }
      )
      .subscribe(
        data => {
          console.log(data);
          loader.dismiss();
          let prompt = this.alertCtrl.create({
            title: "Success",
            message: data["Successfully edited ride."],
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
        },
        error => {
          loader.dismiss();
          let prompt = this.alertCtrl.create({
            title: "Error",
            message: "Error 500 Server Not Found" + error,
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
          console.log(error);
        }
      );
  }

  viewPublicProfile(id: any) {
    var link = this.global.urlApi + "/getPublicProfile/" + id;

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.push("PublicProfilePage", { user: data });
        },
        error => {
          console.log("Oooops!");
        }
      );
  }
}
