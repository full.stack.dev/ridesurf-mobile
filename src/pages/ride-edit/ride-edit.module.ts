import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideEditPage } from './ride-edit';

@NgModule({
  declarations: [
    RideEditPage,
  ],
  imports: [
    IonicPageModule.forChild(RideEditPage),
  ],
  exports:[RideEditPage]
})
export class RideEditPageModule {}
