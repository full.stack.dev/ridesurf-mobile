import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideSearchPage } from './ride-search';

@NgModule({
  declarations: [
    RideSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(RideSearchPage),
  ],
  exports:[RideSearchPage]
})
export class RideSearchPageModule {}
