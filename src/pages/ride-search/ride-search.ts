import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup  } from '@angular/forms';
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";import { AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the RideSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
  selector: 'page-ride-search',
  templateUrl: 'ride-search.html',
})
export class RideSearchPage {

searchRides: FormGroup;	
  map: any;
  autocomplete_from: any;
  autocomplete_to: any;
  GoogleAutocomplete: any;
  GooglePlaces: any;
  geocoder: any
  autocompleteItemsFrom : any;
  autocompleteItemsTo : any;
  nearbyItems: any = new Array<any>();
  loading: any;
  rideList : any;
  aiportRides:boolean = false;
  airports:any;
  airportSelect:any;
  minDate:any;
  constructor(public navCtrl: NavController,  public zone: NgZone, private formBuilder: FormBuilder,
  		public navParams: NavParams, public global : GlobalProvider, public http: HttpClient,  public toastCtrl: ToastController,
      private alertCtrl: AlertController, public loadingCtrl: LoadingController,private geolocation: Geolocation) {

    this.minDate = new Date().toISOString(); 
    
  	this.rideList = new Array();
  	this.geocoder = new google.maps.Geocoder;
  	let elem = document.createElement("div");
  	this.GooglePlaces = new google.maps.places.PlacesService(elem);
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete_from = {
      input: ''
    };
    this.autocomplete_to = {
      input: ''
    };
    this.autocompleteItemsFrom = [];
    this.autocompleteItemsTo = [];

    this.searchRides = this.formBuilder.group({
      from: [''],
      to: [''],
      date : [''],
    });

  }

  changeStatus(){
    this.airportList();
    if(this.aiportRides)
      this.aiportRides = false;
    else
      this.aiportRides = true;


  }

  airportList(){
    let loader = this.loadingCtrl.create({
            content: "Loading"
          });  
        loader.present();

    var link = this.global.urlApi + "/getAirportList";
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
          loader.dismiss();
          this.airports = res;
        },error => {
          loader.dismiss();
          console.log(error);
          console.log("Oooops!");
        }
      );
    
  }

  getCurrentLocation(){
    let loader = this.loadingCtrl.create({
      content: "Locating..."
    });
    loader.present();
    this.geolocation.getCurrentPosition().then((resp) => {
       var location = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+
                       resp.coords.latitude+","+resp.coords.longitude+"&key=AIzaSyBDjHQ2rDf2qaw1-nNbG2zXhsIG6G5Q-8M";
       this.http
      .get(location)
      .subscribe(
        data => {
          console.log(data);
          this.autocomplete_from.input = data['results'][0]['formatted_address'];
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'There was an error getting your location, please try again',
            buttons: ['Dismiss']
          });
          alert.present();
          console.log(error);
          console.log("Oooops!");
        }
      );                
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }

  searchRidesForm() {
  let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

  loader.present();
  if(this.searchRides.value.from == ''){
      loader.dismiss();
      let alert = this.alertCtrl.create({
        title: 'From Field Empty',
        subTitle: 'Please fill out the from field',
        buttons: ['Dismiss']
      });
      alert.present();
  }else if(this.searchRides.value.to == ''){
    loader.dismiss();
    let alert = this.alertCtrl.create({
        title: 'To Field Empty',
        subTitle: 'Please fill out the to field',
        buttons: ['Dismiss']
      });
      alert.present();
  }else if(this.searchRides.value.date == ''){
    loader.dismiss();
    let alert = this.alertCtrl.create({
        title: 'Date Field Empty',
        subTitle: 'Please fill out the date field',
        buttons: ['Dismiss']
      });
      alert.present();
  }

  var link = this.global.urlApi + "/findRideInfo";

  var headers = new Headers();
  headers.append('Accept', 'application/json');

  this.http.post(link,{
    	"Headers": headers,
    	"from": this.searchRides.value.from,
    	"to" :  this.searchRides.value.to,
    	"date" : this.searchRides.value.date,
      "airport":this.aiportRides
    }).subscribe(data => {
      
      loader.dismiss();
      if(data['action'] == 0){
        loader.dismiss();
         const toast = this.toastCtrl.create({
          message: data['error'],
          duration: 3500
        });
        toast.present();

      }else{

      	 this.navCtrl.push('RideDetailPage', {
           data:data,
           from:this.searchRides.value.from,
           to:this.searchRides.value.to,
           date:this.searchRides.value.date,
           airport:this.aiportRides

         });
       }

		 //localStorage.setItem('rideList' , data);
	}, error => {
     loader.dismiss();
     const toast = this.toastCtrl.create({
      message: 'There was an error searching for the ride',
      duration: 3500
    });
    toast.present();
		 console.log("Oooops!");
	});


  }

  searchAirport(){
    var link = this.global.urlApi + "/getAirportRides";

    if(this.airportSelect == null){
         const toast = this.toastCtrl.create({
          message: 'Please select an Airport',
          duration: 3500
        });
        toast.present();
        return false;
    }
    var headers = new Headers();
    headers.append('Accept', 'application/json');
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });


    loader.present();


    this.http.post(link,{
      "Headers": headers,
      "airport": this.airportSelect,
      "date" : this.searchRides.value.date,

    }).subscribe(data => {
      
      loader.dismiss();
      if(data['action'] == 0){
        loader.dismiss();
         const toast = this.toastCtrl.create({
          message: data['error'],
          duration: 3500
        });
        toast.present();

      }else{
        console.log(this.airportSelect);
         this.navCtrl.push('RideDetailPage', {
           data:data,
           from:this.airportSelect.name,
           to:null,
           date:this.searchRides.value.date,
           airport:true
         });
       }

     //localStorage.setItem('rideList' , data);
  }, error => {
     loader.dismiss();
     const toast = this.toastCtrl.create({
      message: 'There was an error searching for the ride',
      duration: 3500
    });
    toast.present();
     console.log("Oooops!");
  });
  }

  ionViewDidLoad() {

  	this.initMap();
    
  }

  initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: 'roadmap'
  	});

  }
  updateSearchResultsFrom(){

    if (this.autocomplete_from.input == '') {

      this.autocompleteItemsFrom = [];
      return;
    }

    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_from.input },
      (predictions, status) => {
        this.autocompleteItemsFrom = [];

        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {

              this.autocompleteItemsFrom.push(prediction);

            });
          });
        }
    });

    
  }

  updateSearchResultsTo(){
    if (this.autocomplete_to.input == '') {
      this.autocompleteItemsTo = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_to.input },
      (predictions, status) => {
        this.autocompleteItemsTo = [];
        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItemsTo.push(prediction);

            });
          });
        }
    });
  }


  selectSearchResultFrom(event: Event, item:any){

    console.log('we call this from at the beggining');
    console.log(this.autocompleteItemsFrom);
  	this.autocomplete_from.input = item.description;
    this.autocompleteItemsFrom = [];

  }

  selectSearchResultTo(item){
  	this.autocomplete_to.input = item.description;
  	this.autocompleteItemsTo = [];

  }


}
