import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  App,
  Platform,
  ToastController,
  Events
} from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Base64 } from '@ionic-native/base64';
import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';
import { ImageTransform } from 'ngx-image-cropper';

@IonicPage()
@Component({
  selector: 'page-driver-apply',
  templateUrl: 'driver-apply.html'
})
export class DriverApplyPage {
  driverInfo: any;
  user: any;
  preferences: any;
  age: any = null;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  canvasRotation = 0;
  rotation = 0;
  scale = 1;
  transform: ImageTransform = {};
  profileImage: any;
  imageCrop: boolean = false;
  getImage: boolean = false;
  registration: any = null;
  device: any;
  role: any;
  options: CameraOptions = {
    quality: 60,
    sourceType: 1,
    saveToPhotoAlbum: false,
    correctOrientation: true,
    destinationType: this.camera.DestinationType.DATA_URL,
    targetHeight: 500
  };
  clickedImagePath: any;
  isDisabled: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public http: HttpClient,
    public userProvider: UserProvider,
    private camera: Camera,
    public global: GlobalProvider,
    public toastCtrl: ToastController,
    public app: App,
    public base64: Base64,
    public events: Events
  ) {
    this.driverInfo = this.userProvider.currentUser();
    this.user = this.driverInfo.user;
    if(this.driverInfo.driver){
      this.role = 'Driver';
      this.clickedImagePath = this.global.url + this.user.driver[0].registration_path;
    }else{
      this.role = 'User';
    }
    if (this.role == null) {
      this.role == 'User';
    }
    if (this.platform.is('ios')) {
      this.device = 'ios';
    } else {
      this.device = 'android';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverApplyPage');
  }

  takePhoto() {
    this.camera.getPicture(this.options).then(imagePath => {
      this.profileImage = 'data:image/jpeg;base64,' + imagePath;
      this.imageCrop = true;
    });
  }

  /**
   * NGX Cropper w/ Rotation
   * @param image
   */
  imageCropped(image: string) {
    this.croppedImage = image;
  }

  imageLoaded() {
    console.log('image load:');
  }

  loadImageFailed() {
    console.log('image load failed:');
  }

  rotateLeft() {
    this.canvasRotation--;
    this.flipAfterRotate();
  }

  rotateRight() {
    this.canvasRotation++;
    this.flipAfterRotate();
  }

  private flipAfterRotate() {
    const flippedH = this.transform.flipH;
    const flippedV = this.transform.flipV;
    this.transform = {
      ...this.transform,
      flipH: flippedV,
      flipV: flippedH
    };
  }

  flipHorizontal() {
    this.transform = {
      ...this.transform,
      flipH: !this.transform.flipH
    };
  }

  flipVertical() {
    this.transform = {
      ...this.transform,
      flipV: !this.transform.flipV
    };
  }

  updateRotation() {
    this.transform = {
      ...this.transform,
      rotate: this.rotation
    };
  }

  exitCropper() {
    this.imageCrop = false;
  }

  saveCroppedImage() {
    let loader = this.loadingCtrl.create({
      content: 'Uploading...'
    });
    loader.present();
    this.imageCrop = false;
    let self = this;
    let url = '/saveRegistration';
    let params = {
      registration: this.croppedImage.base64
    };
    this.userProvider.imageUpload(url, params, function(res: any) {
      loader.dismiss();
      self.isDisabled = false;
      if (res && res.status) {
        self.clickedImagePath = self.global.url + res.data;
      }
    });
  }

  nextPage() {
    this.navCtrl.push('DriverApplyPage2Page', {
      driverInfo: this.driverInfo,
      driver: this.driverInfo,
      role: this.role
    });
  }
}
