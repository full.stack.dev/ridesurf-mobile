import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { DriverApplyPage } from './driver-apply';
import { ComponentsModule } from '../../components/components.module';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [
    DriverApplyPage,
  ],
  imports: [
  	CommonModule,
  	ComponentsModule,
  	ImageCropperModule,
    IonicPageModule.forChild(DriverApplyPage),
  ],
  exports:[DriverApplyPage]
})
export class DriverApplyPageModule {}
