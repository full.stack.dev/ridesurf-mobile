import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenRidePage } from './open-ride';

@NgModule({
  declarations: [
    OpenRidePage,
  ],
  imports: [
    IonicPageModule.forChild(OpenRidePage),
  ],
  exports:[OpenRidePage]
})
export class OpenRidePageModule {}
