import { Component,ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Select, ToastController } from "ionic-angular";
import { FormBuilder, FormGroup } from "@angular/forms";
import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController } from "ionic-angular";
import { Geolocation } from '@ionic-native/geolocation';
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { AlertController } from "ionic-angular";
/**
 * Generated class for the OpenRidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
  selector: 'page-open-ride',
  templateUrl: 'open-ride.html',
})
export class OpenRidePage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild('openHours') select1: Select;

  location_from:any;
  location_to:any; 
  map: any;
  city_from:any;
  city_to:any;
  autocomplete_from: any;
  autocomplete_to: any;
  duration:any;
  purposes:any;
  autocompleteItemsFrom : any;
  autocompleteItemsTo : any;
  GoogleAutocomplete: any;
  GooglePlaces: any;
  geocoder:any;
  allowContinue: boolean = true;
  pricePerSeat:any;
  continue: boolean = false;
  minDate:any;
  open_date_from:any;
  open_date_to:any;
  seats:any;
  luggage_size:any = 'Small';
  luggage_amount:any;
  description:any = null;
  trip_purpose_id:any = 1;
  hours:any;
  OpenHours:any = [];
  totalDistance:any;
  firstslide:boolean = true;
  notFirstSlide:boolean = false;
  slideCounter:any = 1;
  comingFromlastPage:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient,
    public userProvider: UserProvider, public global: GlobalProvider, private formBuilder: FormBuilder,
    public zone: NgZone, private geolocation: Geolocation, public loadingController: LoadingController,
    public alertCtrl: AlertController, public toastCtrl: ToastController) {

    this.geocoder = new google.maps.Geocoder;
    let elem = document.createElement("div");
    this.GooglePlaces = new google.maps.places.PlacesService(elem);
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

    this.autocomplete_from = {
      input: ''
    };
    this.autocomplete_to = {
      input: ''
    };
    this.hours = [
    	{time: '1:00 AM'}, {time: '2:00 AM'}, {time: '3:00 AM'}, {time: '4:00 AM'},
        {time: '5:00 AM'}, {time: '6:00 AM'}, {time: '7:00 AM'}, {time: '8:00 AM'},
        {time: '9:00 AM'}, {time: '10:00 AM'}, {time: '11:00 AM'}, {time: '12:00 AM'},
        {time: '1:00 PM'}, {time: '2:00 PM'}, {time: '3:00 PM'}, {time: '4:00 PM'},
        {time: '5:00 PM'}, {time: '6:00 PM'}, {time: '7:00 PM'}, {time: '8:00 PM'},
        {time: '9:00 PM'}, {time: '10:00 PM'}, {time: '11:00 PM'}, {time: '12:00 PM'}
    ];

    this.autocompleteItemsFrom = [];
    this.autocompleteItemsTo = [];

    this.minDate = new Date().toISOString(); 
  	this.getPurposes();
  	this.seats = 1;
  	this.luggage_amount = 1;

  }

  ionViewDidLoad() {
  	this.initMap();
    console.log('ionViewDidLoad OpenRidePage');
  }

  getPurposes(){
  	var link = this.global.urlApi + "/fetchPurposes";
    this.http.get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(res =>  {
         console.log(res);
         this.purposes = res;
       },error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  initMap() {

    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13,
      mapTypeId: 'roadmap'
    });


  }

  updateSearchResultsFrom(){
    if (this.autocomplete_from.input == '') {
      this.autocompleteItemsFrom = [];
      return;
    }

    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_from.input },
      (predictions, status) => {
        this.autocompleteItemsFrom = [];

        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItemsFrom.push(prediction);
            });
          });
        }
    });

    
  }

  updateSearchResultsTo(){
    if (this.autocomplete_to.input == '') {
      this.autocompleteItemsTo = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_to.input },
      (predictions, status) => {
        this.autocompleteItemsTo = [];
        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItemsTo.push(prediction);

            });
          });
        }
    });
  }

  selectSearchResultFrom(event: Event, item:any){
    this.autocomplete_from.input = item.description;
    this.autocompleteItemsFrom = [];
    this.location_from = item.description;
  }

  selectSearchResultTo(item){
    this.autocomplete_to.input = item.description;
    this.autocompleteItemsTo = [];
    this.location_to = item.description;
  }

  add(number){
    if(this.pricePerSeat < 500)
      this.pricePerSeat++;
  }
  sub(number){
    if(this.pricePerSeat > 2)
      this.pricePerSeat--;
  }
  addSeats(){
  	if(this.seats < 6)
  		this.seats++;
  }
  subSeats(){
  	if(this.seats > 1)
  		this.seats--;
  }
  addLuggage(){
  	if(this.luggage_amount < 6)
  		this.luggage_amount++;
  }
  subLuggage(){
  	if(this.luggage_amount > 1){
  		this.luggage_amount--;
  	}
  }



  getCurrentLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
       var location = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+
                       resp.coords.latitude+","+resp.coords.longitude+"&key=AIzaSyAMi7bbbmmewyAcfg5_ATg-bjb8FCo7De4";
       this.http
      .get(location)
      .subscribe(
        data => {
          console.log(data);
          this.autocomplete_from.input = data['results'][0]['formatted_address'];
          this.location_from = data['results'][0]['formatted_address'];
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );                
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }

  startNavigating(){

        let loader = this.loadingController.create({
            content: "Loading"
          });  
        loader.present();
        let directionsService = new google.maps.DirectionsService;
        let directionsDisplay = new google.maps.DirectionsRenderer;
        let waypts = [];

        directionsDisplay.setMap(this.map);

        directionsService.route({
            origin: this.autocomplete_from.input,
            destination: this.autocomplete_to.input,
            travelMode: google.maps.TravelMode['DRIVING'],
            waypoints: waypts,
            optimizeWaypoints: true,
        }, (res, status) => {

            if(status == google.maps.DirectionsStatus.OK){
                directionsDisplay.setDirections(res);

                var last = res.routes[0].legs.length - 1;

                this.duration = 0;
                this.totalDistance = 0; 

                for(var j=0; j <= last; j++){
                  this.totalDistance = this.totalDistance + res.routes[0].legs[j].distance.value;
                  this.duration = this.duration + res.routes[0].legs[j].duration.value;
                }

                this.city_from = this.sliptCity(this.autocomplete_from.input);

                this.city_to = this.sliptCity(this.autocomplete_to.input);

                this.totalDistance = Math.ceil(this.totalDistance * 0.000621371192);
                this.pricePerSeat = Math.ceil(this.totalDistance * this.global.pricePerMile);
                
                this.allowContinue = false;
                this.continue = true;
                loader.dismiss();
            } else {
                const toast = this.toastCtrl.create({
                      message: 'Error trying to route, please try again',
                      duration: 2500,
                });
                toast.present();
                console.warn(status);
                loader.dismiss();
            }

        });

    }

    sliptCity(city:any){
      var temp = city.split(",");

      if(temp.length == 3){
        return temp[0];
      }
      if(temp.length == 4){
        return temp[1];
      }
      else{
        return city;
      }

    }

    nextPage(){
    	
    	this.allowContinue = true;
      this.firstslide = false;
      this.notFirstSlide = true;
      this.slideCounter++;
      this.slides.slideNext();
    }
    backPage(){
    	
      console.log(this.slideCounter);
      if(this.slideCounter == 2){
        this.firstslide = true;
        this.notFirstSlide = false;
      }
      if(this.slideCounter == 4){
        console.log('We are in last page')
        this.comingFromlastPage = true;
      }
      this.allowContinue = false;
      this.slideCounter--;
      this.slides.slidePrev();
    }

    //Allow to continue to next page when all the
    //Dates are inputed
    allowToContinue(date){

    	if(this.comingFromlastPage){
        this.allowContinue = false;
      }
      console.log(this.OpenHours.length);
      console.log(this.OpenHours);
    	if(this.open_date_from != null && this.open_date_to != null && this.OpenHours.length > 0){
    		  this.allowContinue = false;
    	}
    }

    openSelect(){
    	console.log('selected');
    	setTimeout(() => {
	       this.select1.open();
	    },150);  
    }

    uploadRide(){
    	
	    if(this.open_date_to <= this.open_date_from){
	    	let prompt = this.alertCtrl.create({
	            title: "Error",
	            message: " Your end date cannot be before your start date",
	            buttons: [
	              {
	                text: "Close",
	                handler: data => {}
	              }
	            ]
	          });
	          prompt.present();
	    	return false;
	    }
      if(this.description == null){
        let prompt = this.alertCtrl.create({
              title: "Error",
              message: "Please fill out a description",
              buttons: [
                {
                  text: "Close",
                  handler: data => {}
                }
              ]
            });
          prompt.present();
        return false;
      }

      let loader = this.loadingController.create({
        content: "Creating"
      });
      loader.present();

	    var link = this.global.urlApi + "/createOpenRides";
	    var data = {
	      open_date_from: this.open_date_from,
	      open_date_to: this.open_date_to,
	      seats: this.seats,
	      price: this.pricePerSeat,
	      luggage_size: this.luggage_size,
	      luggage_amount: this.luggage_amount,
	      description: this.description,
	      location_from: this.location_from,
	      location_to: this.location_to,
	      duration: this.duration,
	      trip_purpose_id:this.trip_purpose_id,
	    };
	    console.log(data);

		this.http.post(link,
        {
          data: data,
          openHours:this.OpenHours
        },
        {
          headers: new HttpHeaders()
            
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      	).subscribe(
        data => {
          console.log(data);
          loader.dismiss();
          this.navCtrl.setRoot("SuccessCreateRidePage", { 
            data: data["message"],
            type:2,
             });
          
        },error => {
          loader.dismiss();
          console.log(error);
          let prompt = this.alertCtrl.create({
            title: "Error",
            message: "Error " + error.error.message,
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
          console.log(error);
        }
      );
    }
}
