
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { App } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';

import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the CurrentRidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-current-rides",
  templateUrl: "current-rides.html"
})
export class CurrentRidesPage {


  rides:any = [];
  currentUser:any;
  page:any = 1;
  lastPage:any;
  driver:any;
  indexStart:any;
  lastIndex:any;
  constructor( public navCtrl: NavController, public navParams: NavParams ,public userProvider: UserProvider,
    public http: HttpClient,  public global : GlobalProvider, public app:App) {
    
    this.getCurrentRides();
    this.currentUser = this.userProvider.currentUser();
    this.driver = this.currentUser.driver;

  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CurrentRidesPage");
  }

  doInfinite(infiniteScroll) {
    this.page++;
    this.getCurrentRides(infiniteScroll);
    if (this.page == this.lastPage) {
      infiniteScroll.enable(false);
    }
  }

  getCurrentRides(infiniteScroll?) {
    var link = this.global.urlApi + "/getCurrentRides/" + this.page;
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        res => {
          if (res["data"] != null && res["to"] != null) {
              this.rides = this.rides.concat(res["data"]);
            
            console.log(res);
            this.lastPage = res["last_page"];
            if (infiniteScroll) {
              infiniteScroll.complete();
            }
          }
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  editRide(index: any) {
    this.navCtrl.push('RideEditPage', { data: this.rides[index] });
  }

  deleteRide(id: any) {
    this.navCtrl.push('DeleteRidePage', { data: id });
  }

  driverApply() {
    var link = this.global.urlApi + "/gettingDriverInfo";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.push('DriverApplyPage', { data: data });
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );

  }

  arrangeArray(array:any){
    let newArray = [];
    var j =0;
    if(this.indexStart == this.lastIndex){
      
      if(this.indexStart == 1){
        console.log('uses the first statement');
        return newArray = array[this.indexStart];
       }else{
         return newArray = array;
       }
    }
    console.log('uses loop');
    for(var i = this.indexStart; i < this.lastIndex; i++ ){
      newArray[j++] = array[i];
    }
    return newArray;
  }
}
