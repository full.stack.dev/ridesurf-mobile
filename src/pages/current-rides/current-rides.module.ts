import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrentRidesPage } from './current-rides';

@NgModule({
  declarations: [
    CurrentRidesPage,
  ],
  imports: [
    IonicPageModule.forChild(CurrentRidesPage),
  ],
  exports:[CurrentRidesPage]
})
export class CurrentRidesPageModule {}
