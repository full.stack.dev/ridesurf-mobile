import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditOpenRidePage } from './edit-open-ride';

@NgModule({
  declarations: [
    EditOpenRidePage,
  ],
  imports: [
    IonicPageModule.forChild(EditOpenRidePage),
  ],
  exports:[EditOpenRidePage]
})
export class EditOpenRidePageModule {}
