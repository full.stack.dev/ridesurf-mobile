import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Select, ToastController } from 'ionic-angular';
import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController, AlertController } from "ionic-angular";
import { Geolocation } from '@ionic-native/geolocation';
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
/**
 * Generated class for the EditOpenRidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-open-ride',
  templateUrl: 'edit-open-ride.html',
})
export class EditOpenRidePage {
  @ViewChild('openHours') select1: Select;

  ride:any;
  OpenHours:any;
  hours:any;
  purposes:any;
  hourSwtich: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController,
    public userProvider: UserProvider, public global: GlobalProvider, public loadingController: LoadingController) {
  	this.ride = this.navParams.get('openRide');
  	this.OpenHours = this.ride['hours'];
    console.log(this.ride);

  	this.hours = [
    	  {hour: '1:00 AM'}, {hour: '2:00 AM'}, {hour: '3:00 AM'}, {hour: '4:00 AM'},
        {hour: '5:00 AM'}, {hour: '6:00 AM'}, {hour: '7:00 AM'}, {hour: '8:00 AM'},
        {hour: '9:00 AM'}, {hour: '10:00 AM'}, {hour: '11:00 AM'}, {hour: '12:00 AM'},
        {hour: '1:00 PM'}, {hour: '2:00 PM'}, {hour: '3:00 PM'}, {hour: '4:00 PM'},
        {hour: '5:00 PM'}, {hour: '6:00 PM'}, {hour: '7:00 PM'}, {hour: '8:00 PM'},
        {hour: '9:00 PM'}, {hour: '10:00 PM'}, {hour: '11:00 PM'}, {hour: '12:00 PM'}
    ];
  }

  openSelect(){
  	setTimeout(() => {
         this.select1.open();
      },150);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditOpenRidePage');
  }

  addLuggage(){
  	if(this.ride.luggage_amount < 5)
  		this.ride.luggage_amount++;
  }

  subLuggage(){
  	if(this.ride.luggage_amount > 0 )
  	this.ride.luggage_amount--;
  }

  subPrice(){
  	if(this.ride.price > 0)
  		this.ride.price--;
  }

  addPrice(){
  	this.ride.price++;
  }

  subSeats(){
  	if(this.ride.seats > 0)
  	this.ride.seats--;
  }

  addSeats(){
  	if(this.ride.seats < 6)
  		this.ride.seats++;
  }

  onChange(){
    this.hourSwtich = true;
  }

  getPurposes(){
  	var link = this.global.urlApi + "/fetchPurposes";
    this.http.get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(res => {
         console.log(res);
         this.purposes = res;
       },error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  editRide(){
    var data = {
        open_date_from: this.ride.open_date_from,
        open_date_to: this.ride.open_date_to,
        seats: this.ride.seats,
        price: this.ride.pricePerSeat,
        luggage_size: this.ride.luggage_size,
        luggage_amount: this.ride.luggage_amount,
        description: this.ride.description,
        location_from: this.ride.location_from,
        location_to: this.ride.location_to,
        duration: this.ride.duration,
        trip_purpose_id:this.ride.trip_purpose_id,
        hourSwtich : this.hourSwtich,
      };
    let loader = this.loadingController.create({
        content: "Updating..."
      });
      loader.present();
    var link = this.global.urlApi +'/editOpenRide/'+this.ride.id;
    this.http.post(link,
        {
          data: data,
          openHours:this.OpenHours
        },
        {
          headers: new HttpHeaders()
            
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
        ).subscribe(
        data => {
          console.log(data);
          loader.dismiss();
          let prompt = this.alertCtrl.create({
            title: "Success",
            message: "Your ride has been updated",
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
          
        },error => {
          loader.dismiss();
          console.log(error);
          let prompt = this.alertCtrl.create({
            title: "Error",
            message: "Error " + error.error,
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
          console.log(error);
        }
      );

  }

}
