import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { RidesBookedPage } from '../rides-booked/rides-booked';

/**
 * Generated class for the CancelRidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cancel-ride',
  templateUrl: 'cancel-ride.html',
})
export class CancelRidePage {

  user_ride:any;
  reason:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
  		      public http: HttpClient,  public global : GlobalProvider, public toastCtrl: ToastController) {

  	this.user_ride = this.navParams.get('ride');
    console.log(this.user_ride.control);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CancelRidePage');
  }

  //Cancel Ride from User(passenger)
  cancelRide(){
    var link = this.global.urlApi + "/deleteUserRide";
    let loader = this.loadingCtrl.create({
      content: "Deleting..."
    });

    loader.present();
    this.http.post(link,{
      uRide_id:this.user_ride.id,
      reason:this.reason,
    }, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data  => {
           loader.dismiss();
           const toast = this.toastCtrl.create({
              message: data['message'],
              duration: 3500
            });
            toast.present();
            this.navCtrl.setRoot( RidesBookedPage );
        }, error => {
         console.log(error);
         console.log("Oooops!");
    });
  }

}
