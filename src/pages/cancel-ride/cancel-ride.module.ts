import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelRidePage } from './cancel-ride';

@NgModule({
  declarations: [
    CancelRidePage,
  ],
  imports: [
    IonicPageModule.forChild(CancelRidePage),
  ],
  exports:[CancelRidePage]
})
export class CancelRidePageModule {}
