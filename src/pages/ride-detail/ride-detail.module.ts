import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideDetailPage } from './ride-detail';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    RideDetailPage,
  ],
  imports: [
  	StarRatingModule,
    IonicPageModule.forChild(RideDetailPage),
  ],
  exports:[RideDetailPage]
})
export class RideDetailPageModule {}
