import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,
        LoadingController, Content,ToastController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { FilterComponent } from '../../components/filter/filter';

/**
 * Generated class for the RideDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride-detail',
  templateUrl: 'ride-detail.html',
})
export class RideDetailPage {

  @ViewChild(Content) content: Content;

  rideList:any;
  url:any;
  urlApi:any;
  action:any;
  currentUser: any;
  from:any;
  to:any;
  date:any;
  messageUser:any =[];
  openRides:any;
  airport:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global : GlobalProvider,
    public http: HttpClient, public userProvider: UserProvider, public loadingCtrl:LoadingController,
    public toastCtrl: ToastController, public storage:Storage, public modalCtrl:ModalController,
    public alertCtrl:AlertController) {

    this.url = this.global.url;
    this.urlApi = this.global.urlApi;

    //Get data from current search
    var data = this.navParams.get('data');
    this.rideList = data['rides'];
    this.openRides = data['openRides'];
    this.action = data['action'];
    this.airport =  this.navParams.get('airport');
    var temp_from = this.navParams.get('from').split(",");
    this.from = this.sliptCity(this.navParams.get('from')); 
    this.date = this.navParams.get('date');

    
    if(!this.airport){
      
      var temp_to = this.navParams.get('to').split(",");
      this.to = this.sliptCity(this.navParams.get('to')); 
      
    }
    

    this.storage.get('userData').then((val) => {
          this.currentUser = val;
    });

    
  }

  sliptCity(city:any){
      var temp = city.split(",");

      if(temp.length == 3){
        return temp[0] + ", " + temp[1];
      }
      if(temp.length == 4){
        return temp[1] + ", " + temp[2];
      }
      else{
        return city;
      }

    }

  bookRide(ride_id:any, ladies:any){

    if(this.currentUser.user.gender == 'Male' && ladies == 'Yes'){
      const toast = this.toastCtrl.create({
        message: 'This is a women only ride.',
        duration: 4000
      });
      toast.present();

      return;
    }

    this.messageUser = this.validateUser();

    if(this.messageUser.length == 0){
      var link = this.global.urlApi + "/rideBooking";

      var headers = new Headers();
      headers.append('Accept', 'application/json ');

      this.http.post(link,{
          "ride_id" :ride_id,
        },
        {
          headers: new HttpHeaders()
                .set("Content-Type", "Application/json")
                .set("Accept", "application/json")
                .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           console.log('Success search');
           this.navCtrl.push('RideBookingPage', {data : data, from: this.from, to: this.to});
      }, error => {
         console.log("Oooops!");
      });
    }else{
      //Show why the user can't book this ride yet
      let prompt = this.alertCtrl.create({
            title: "You need to provide this info on your profile before booking:",
            message: this.messageUser,
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
        return;
    }

  }

  rideReminder(){
    this.navCtrl.push('RideReminderPage', {
      from: this.from, 
      to: this.to, 
      date: this.date,
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RideDetailPage');
  }

  viewPublicProfile(user_id:any){

    var link  = this.global.urlApi + "/getPublicProfile/" + user_id;

    this.http.get(link,{
          headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           console.log(data);
           this.navCtrl.push('PublicProfilePage', {user : data})
         //localStorage.setItem('rideList' , data);
      }, error => {
         console.log("Oooops!");
      });
    
  }

  swapPlaces(){
    let loader = this.loadingCtrl.create({
        content: "Loading..."
      });

    loader.present();

    var link = this.global.urlApi + "/findRideInfo";

    var headers = new Headers();
    headers.append('Accept', 'application/json ');

    this.http.post(link,{
        "Headers": headers,
        "from": this.to,
        "to" :  this.from,
        "date" : this.date
      }).subscribe(data => {
        //this.navCtrl.pop();
        loader.dismiss();
        this.navCtrl.setRoot(RideDetailPage, {
         data : data, 
         from:this.to,
         to:this.from,
         date:this.date,
       });
    }, error => {
      loader.dismiss();
       console.log("Oooops!");
    });
  }

  filter(){
  
    console.log(this.rideList);
    if(this.action == 2){
      var price = 0;
    }else{
      if(this.rideList == null){
        price = 2;
      }else{
        price = this.rideList[0].price;
      }
      
    }
    let messageModal = this.modalCtrl.create(FilterComponent, {
      price : price,
      from: this.from,
      to :  this.to,
      date : this.date,
      airport: this.airport
    });
    messageModal.present();
  }

  validateUser(){
    var i = 0;
    var message = [];
    if(this.currentUser.user.verified == false){
      message[i++] = "Email Address \n";
    }
    if(this.currentUser.user.address == null){
      message[i++] = "Mailing Address \n";
    }
    if(this.currentUser.user.phone_number == null){
      message[i++] = "Phone Number \n";
    }
    if(this.currentUser.user.gender == null){
      message[i++] = "Gender \n";
    }
    return message;
  }

  bookOpenRide(ride:any){
      console.log(this.currentUser);
      if(this.currentUser.user_id == ride.user_id){
        const toast = this.toastCtrl.create({
          message: "You can't book your own ride",
          duration: 4000
        });
        toast.present();
      }else{
        this.navCtrl.push('OpenRideBookPage', 
        {ride : ride });
      }
      
      
  }

}
