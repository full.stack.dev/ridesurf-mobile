import { Component, ElementRef } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  AlertController,
  LoadingController,
  ToastController
} from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';
import { RestProvider } from '../../providers/rest/rest';

@IonicPage()
@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html'
})
export class ProfileEditPage {
  profileForm: FormGroup;
  currentUser: any;
  years: any = [];
  response: any;
  states: any;
  countries: any;
  index: any;
  countries_code: any;
  area_code: any;
  isUsa: boolean = false;
  flagUrl: any;
  isInternational: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public userProvider: UserProvider,
    public http: HttpClient,
    public alertCtrl: AlertController,
    public rest: RestProvider,
    public global: GlobalProvider,
    public events: Events,
    public element: ElementRef,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {
    this.flagUrl = this.global.url + '/flags/';
    this.getCountries();
    this.years = Array.from(Array(62).keys()).map(x => 2001 - x);
    this.currentUser = this.userProvider.currentUser();
    if (this.currentUser.user.area_code != null) {
      this.area_code = '+' + this.currentUser.user.area_code;
    } else {
      this.area_code = '+1';
    }

    if (this.currentUser.user.area_code != 1) {
      this.isInternational = true;
    }

    this.events.subscribe('get-area-code', area => {
      /// Do stuff with "paramsVar"
      this.area_code = '+' + area;
      if (area != 1) {
        this.isInternational = true;
      }
    });

    this.profileForm = this.formBuilder.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone_number: ['', [Validators.required]],
      temp_area: [''],
      area_code: [this.area_code],
      address: [''],
      city: [''],
      country: [this.currentUser.user.country, [Validators.required]],
      state: ['', [Validators.required]],
      description: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      birthday: ['']
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileEditPage');
    if (!this.currentUser) {
      this.currentUser = this.userProvider.currentUser();
    }
    if (this.currentUser.user.country == 'United States') {
      this.isUsa = true;
      this.updateStates();
    }
  }

  myCallbackFunction = _params => {
    return new Promise((resolve, reject) => {
      this.area_code = _params;
      resolve();
    });
  };

  // push page...
  openCountries() {
    this.navCtrl.push('CountryListPage', {
      callback: this.myCallbackFunction
    });
  }

  saveProfileData() {
    if (this.profileForm.invalid) {
      let toast = this.toastCtrl.create({
        message: 'Please input all required data!',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return;
    }

    var data = {
      first_name: this.profileForm.value.firstname,
      last_name: this.profileForm.value.lastname,
      email: this.profileForm.value.email,
      phone_number: this.profileForm.value.phone_number,
      area_code: this.area_code,
      address: this.profileForm.value.address,
      city: this.profileForm.value.city,
      country: this.profileForm.value.country,
      state: this.profileForm.value.state,
      description: this.profileForm.value.description,
      gender: this.profileForm.value.gender,
      birthday: this.profileForm.value.birthday
    };

    let loader = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loader.present();

    setTimeout(() => {
      this.response = this.userProvider.saveProfileData(data);
      this.userProvider.updateProfile();
      this.currentUser = this.userProvider.currentUser();
      loader.dismiss();
    }, 3000);
  }

  adjust($element) {
    const ta = this.element.nativeElement.querySelector('textarea');
    if (ta !== undefined && ta !== null) {
      ta.style.overflow = 'hidden';
      ta.style.height = 'auto';
      ta.style.height = ta.scrollHeight + 'px';
    }
  }

  getCountries() {
    let link = this.global.urlApi + '/getCountries';
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set('Content-Type', 'Application/json')
          .set('Accept', 'application/json')
      })
      .subscribe(
        data => {
          this.countries = data;
          this.index = this.countries.findIndex(
            x => x.name === this.currentUser.user.country
          );
        },
        error => {
          console.log(error);
          console.log('Oooops!');
        }
      );
  }

  updateStates() {
    if (
      this.profileForm.value.country == 'United States' ||
      this.isUsa == true
    ) {
      let link = this.global.urlApi + '/getStates';
      this.http
        .get(link, {
          headers: new HttpHeaders()
            .set('Content-Type', 'Application/json')
            .set('Accept', 'application/json')
        })
        .subscribe(
          data => {
            this.states = data;
            this.isUsa = true;
          },
          error => {
            console.log(error);
            console.log('Oooops!');
          }
        );
    }
  }
}
