import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenRideBookPage } from './open-ride-book';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    OpenRideBookPage,
  ],
  imports: [
  	StarRatingModule,
    IonicPageModule.forChild(OpenRideBookPage),
  ],
  exports:[OpenRideBookPage]
})
export class OpenRideBookPageModule {}
