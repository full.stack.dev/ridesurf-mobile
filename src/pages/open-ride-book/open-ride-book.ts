import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Select, LoadingController, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the OpenRideBookPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-open-ride-book',
  templateUrl: 'open-ride-book.html',
})
export class OpenRideBookPage {
  @ViewChild('seatsSelect') selectRef: Select;

  ride:any;	
  preferences:any;
  seatNum:any;
  request_description:any;
  seats : any = [];
  dates:any =[];
  hours:any = [];
  date_request:any;
  hour_request:any;
  url:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController,
    public global : GlobalProvider, public http:HttpClient, public loadingCtrl: LoadingController) {
  	this.url = this.global.url;
  	this.ride = this.navParams.get('ride');
    for(var i=0; i < this.ride.seats; i++){
      this.seats[i] = i+1; 
    }
    this.seatNum = 1;
  	this.getPreferences();
  	this.getDates();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenRideBookPage');
  }

  bookNow(){
  	if(!this.date_request){
  		let prompt = this.alertCtrl.create({
            title: 'Error',
            message: 'You are missing the date',
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
        return;
  	}
  	if(this.hour_request == ''){
  		let prompt = this.alertCtrl.create({
            title: 'Error',
            message: 'You are missing the hour',
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
        return;
  	}
  	if(!this.seatNum){
  		let prompt = this.alertCtrl.create({
            title: 'Error',
            message: 'You are missing the amount of seats',
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
        return;
  	}
  	let loader = this.loadingCtrl.create({
        content: "Loading..."
      });

    loader.present();
  	var link = this.global.urlApi + "/bookOpenRide/" + this.ride.id;

  	this.http.post(link,
  		{
  			date:this.date_request,
  			hour:this.hour_request,
  			seat:this.seatNum,
  			description:this.request_description,
  		}
  		,{
        headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
        	loader.dismiss();
        	this.navCtrl.setRoot('SuccessRequestPage', {data : data});
      }, error => {
      		loader.dismiss();
      		let prompt = this.alertCtrl.create({
	            title: 'Error',
	            message: 'There was an error requesting', 
	            buttons:
	              [{
	                  text: 'Close',
	                  handler: data => {

	                  }
	              }]
	          });
	      prompt.present();
         	console.log("Oooops!");
      });

  }  

  getDates(){
  	var link = this.global.urlApi + "/getOpenRideDates/" + this.ride.id;

    this.http.get(link,{
        headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           this.dates = data['dates'];
           this.hours = data['hours']
           this.date_request = this.dates[0];
           this.hour_request = this.hours[0];
           console.log(data);
         //localStorage.setItem('rideList' , data);
      }, error => {
         console.log("Oooops!");
      });
  }

  getPreferences(){
    var link = this.global.urlApi + "/getOtherUserPreferences/" + this.ride.user.id;
    this.http.get(link,{
        headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           this.preferences = data;
           console.log(data);
         //localStorage.setItem('rideList' , data);
      }, error => {
         console.log("Oooops!");
      });
  }


  viewPublicProfile(id:any){

    var link  = this.global.urlApi + "/getPublicProfile/" + id;

    this.http.get(link,{
        headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           console.log(data);
           this.navCtrl.push( 'PublicProfilePage', { 
             user : data, 
             preferences: data['preferences'],
             sameProfile : false,
        })
         //localStorage.setItem('rideList' , data);
      }, error => {
         console.log("Oooops!");
      });
    
  }

}
