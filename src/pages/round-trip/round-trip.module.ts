import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoundTripPage } from './round-trip';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    RoundTripPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(RoundTripPage),
  ],
  exports:[RoundTripPage]
})
export class RoundTripPageModule {}
