import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrentBookedPage } from './current-booked';

@NgModule({
  declarations: [
    CurrentBookedPage,
  ],
  imports: [
    IonicPageModule.forChild(CurrentBookedPage),
  ],
  exports:[CurrentBookedPage]
})
export class CurrentBookedPageModule {}
