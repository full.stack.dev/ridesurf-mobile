import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';
import { AlertController } from 'ionic-angular';
import { CreateMessageComponent } from '../../components/create-message/create-message';
import { UserProvider } from "../../providers/user/user";

/**
 * Generated class for the CurrentBookedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-current-booked',
  templateUrl: 'current-booked.html',
})
export class CurrentBookedPage {
  
  rides:any = [];
  url:any;
  page:any = 0;
  lastPage:any;
  indexStart:any;
  lastIndex:any;
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams , public modalCtrl: ModalController,
  	public http: HttpClient,  public global : GlobalProvider, public alertCtrl:AlertController, public userProvider:UserProvider) {
    this.user = this.userProvider.currentUser();
    this.url = this.global.url;
  	this.getCurentBooked();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurrentBookedPage');
  }

  doInfinite(infiniteScroll) {

      this.page++;
      this.getCurentBooked(infiniteScroll);
      if(this.page == this.lastPage){
        infiniteScroll.enable(false);
      }

  }

  getCurentBooked(infiniteScroll?){
  	var link = this.global.urlApi + "/getCurentBooked/" + this.page;

  	this.http.get(link, {
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(res  => {
          if (res["data"] != null && res["to"] != null) {
              this.rides = this.rides.concat(res["data"]);
            
            console.log(res);
            this.lastPage = res["last_page"];
            if (infiniteScroll) {
              infiniteScroll.complete();
            }
          }
      	}, error => {
         console.log(error);
         console.log("Oooops!");
    });
  }

  viewRide(i:any){

    this.navCtrl.push('CurrentBookedViewPage', {ride:this.rides[i]});

  }

  contactDriver(id:any, ride_id:any){

    var link = this.global.urlApi + "/messageDriver/" + id + "/"+ ride_id;

    this.http.get(link, 
    { headers: new HttpHeaders()
            .set("Content-Type", "x-www-form-urlencoded")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer ' + localStorage.getItem('token')),

    }).subscribe(data => {
      if(data != 0){
        this.navCtrl.push('SingleMessagePage', {
          message:data, 
          id: data['id'],
          user:this.user,
        });
      }
      else{
        
        let messageModal = this.modalCtrl.create(CreateMessageComponent, {otherUser: id, ride_id:ride_id});
        messageModal.present();
      }

    }, error => {
      console.log(error);
      let prompt = this.alertCtrl.create({
            title: 'Error',
            message: error.error.message,
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
        prompt.present();
    });

  }

  payRide(ride_id: any, price: any, seats: any) {
    this.navCtrl.push("RidePaymentPage", {
      ride_id: ride_id,
      price: price,
      seats: seats
    });
  }

  cancelRide(ride:any){
    this.navCtrl.push('CancelRidePage', {ride:ride});
  }

}
