import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RideHistoryPage } from '../ride-history/ride-history';
import { HomePage} from '../home/home';
/**
 * Generated class for the SuccessCreateRidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-success-create-ride',
  templateUrl: 'success-create-ride.html',
})
export class SuccessCreateRidePage {
  message:any;
  typeRide:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  	this.message = this.navParams.get('data');
    this.typeRide = this.navParams.get('type')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessCreateRidePage');
  }

  rideHistory(){
  	//this.navCtrl.setRoot(RideHistoryPage, {tabIndex: this.typeRide});
    this.navCtrl.setRoot(HomePage);

  }
}
