import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccessCreateRidePage } from './success-create-ride';

@NgModule({
  declarations: [
    SuccessCreateRidePage,
  ],
  imports: [
    IonicPageModule.forChild(SuccessCreateRidePage),
  ],
  exports:[SuccessCreateRidePage]
})
export class SuccessCreateRidePageModule {}
