import { Component,  } from '@angular/core';
import { IonicPage, NavController, NavParams, App,   } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { LoadingController, AlertController,   } from 'ionic-angular';
import { ActionSheetController } from "ionic-angular";
import { NotificationsPage } from "../notifications/notifications";
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the RidePaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride-payments',
  templateUrl: 'ride-payment.html',
})
export class RidePaymentPage {
  
  ride_id:any;
  price:any;
  totalPrice:any;
  seats:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private payPal: PayPal, public app:App,
  	public http: HttpClient,  public global : GlobalProvider, public loadingController: LoadingController,
  	public alertCtrl : AlertController, public actionSheetCtrl:ActionSheetController) {
  	this.ride_id = navParams.get('ride_id');
  	this.price = navParams.get('price');
  	this.totalPrice = (this.price) + 1.50;
  	console.log(this.price);
  	this.seats = navParams.get('seats');
  }

  ionViewDidLoad() {

  }

  payNow(){

  	// const actionSheet = this.actionSheetCtrl.create({
   //    title: "You will be redirected to our mobile website to pay",
   //    buttons: [
   //      {
   //        text: "Go to Ridesurf Payment",
   //        handler: () => {
   //          var ref = window.open(
   //            "https://ridesurf.io/dashboarduser/dashboarduser",
   //            "_blank",
   //            "location=yes"
   //          );
            
   //          this.navCtrl.pop();

   //          ref.addEventListener('exit', function (event) {
			//     this.navCtrl.pop();
			// });
   //        }
   //      },
   //      {
   //        text: "Cancel",
   //        role: "cancel",
   //        handler: () => {

   //        }
   //      }
   //    ]
   //  });
   //  actionSheet.present();


  	this.payPal.init({
	  PayPalEnvironmentProduction: 'ASW6BK8mGYilUfGKRkSH1YoXQHp8gnGoJxwdj2qlJm_x8t_r0btHnsVAOyIWAU_dzBWBrNjzY2nQl51A',
	  PayPalEnvironmentSandbox: 'Aa-bqaGAlJYzwSub0o0DiXflk475Y2mvPnLjNzV4huHQQHXnGx9hxRODLbPgf9KIQmy_viZSW9ovm-pi'
	}).then(() => {
	  // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
	  this.payPal.prepareToRender('PayPalEnvironmentProduction', new PayPalConfiguration({
	    // Only needed if you get an "Internal Service Error" after PayPal login!
	    //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
	  })).then(() => {
	    let payment = new PayPalPayment( String(this.totalPrice), 'USD', 'Ridesurf Ride',  'sale');
	    this.payPal.renderSinglePaymentUI(payment).then((success) => {
	    let loader = this.loadingController.create({
		    content: "Loading"
		});  
		loader.present();
	      var link = this.global.urlApi + "/paymentSucess";
	      console.log(payment);
	      console.log(success);
		  this.http.post(link,{
		 		"payment":payment,
		    	"success":success,
		    	"seats":this.seats,
		    	"ride_id":this.ride_id,
		    	"price_paid":this.price,
		    },
		    {
		    	headers: new HttpHeaders()
	            .set("Accept", "application/json")
	            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
		    }).subscribe(data => {
		    	loader.dismiss();
		    	console.log(data);
		    	this.app.getRootNav().setRoot('SuccessBookingPage', {
		    		ride : data['ride']
		    	});
			}, error => {
				loader.dismiss();
				let prompt = this.alertCtrl.create({
	            title: "Error",
	            message: "There was an error trying to make the payment, if the problem percist, please contact us." ,
	            buttons: [
		              {
		                text: "Close",
		                handler: data => {}
		              }
		            ]
		          });
		        prompt.present();
				 console.log("Oooops!");
			});

	    }, () => {
	      // Error or render dialog closed without being successful
	    });
	  }, () => {
	    // Error in configuration
	  });
	}, () => {
	  // Error in initialization, maybe PayPal isn't supported or something else
	});
  }

}
