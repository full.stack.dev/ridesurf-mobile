import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RidePaymentPage } from './ride-payment';

@NgModule({
  declarations: [
    RidePaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(RidePaymentPage),
  ],
  exports:[RidePaymentPage]
})
export class RidePaymentPageModule {}
