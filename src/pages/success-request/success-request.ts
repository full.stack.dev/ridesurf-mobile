import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RideHistoryPage } from "../ride-history/ride-history";
import { HomePage } from "../home/home";

/**
 * Generated class for the SuccessRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-success-request',
  templateUrl: 'success-request.html',
})
export class SuccessRequestPage {
  
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.data = navParams.get('data');
  	console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessRequestPage');
  }

  rideHistory(){
    this.navCtrl.setRoot(HomePage);
  }

}
