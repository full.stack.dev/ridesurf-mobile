import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccessRequestPage } from './success-request';

@NgModule({
  declarations: [
    SuccessRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(SuccessRequestPage),
  ],
  exports:[SuccessRequestPage]
})
export class SuccessRequestPageModule {}
