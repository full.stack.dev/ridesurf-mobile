import { NgModule,} from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicPageModule,  } from 'ionic-angular';
import { OneWayPage } from './one-way';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    OneWayPage,
  ],
  imports: [

  	CommonModule,
  	ComponentsModule,
    IonicPageModule.forChild(OneWayPage),
  ],
  exports:[OneWayPage]
})
export class OneWayPageModule {}
