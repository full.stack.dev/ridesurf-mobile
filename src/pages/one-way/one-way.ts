import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Slides } from 'ionic-angular';
import { FormBuilder, FormGroup  } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import * as moment from 'moment';
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
/**
 * Generated class for the OneWayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
  selector: 'page-one-way',
  templateUrl: 'one-way.html',
})
export class OneWayPage {

  @ViewChild(Slides) slides: Slides;


  location_from:any;
  location_to:any;
  searchRides: FormGroup;  
  map: any;
  autocomplete_from: any;
  autocomplete_to: any;
  autocomplete_waypoint: any;
  GoogleAutocomplete: any;
  GooglePlaces: any;
  geocoder: any
  autocompleteItemsFrom : any;
  autocompleteItemsTo : any;
  autocompletewaypoint : any;
  distanceText:any;
  duration:any;
  hideMe:boolean = true;
  showMe:boolean = false;
  waypoints: any = [];
  wayPointsSelected: string = '';
  pricePerSeat:any;
  totalDistance:any;
  stopOvers = [];
  durationText:any;
  city_from:any;
  city_to:any;

  constructor(public navCtrl: NavController, public zone: NgZone, public loadingController: LoadingController,
    private formBuilder: FormBuilder, public navParams: NavParams,  private geolocation: Geolocation,
    public global : GlobalProvider, public toastCtrl: ToastController, public http:HttpClient) {

    this.geocoder = new google.maps.Geocoder;
    let elem = document.createElement("div");
    this.GooglePlaces = new google.maps.places.PlacesService(elem);
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

    this.autocomplete_from = {
      input: ''
    };
    this.autocomplete_to = {
      input: ''
    };

    this.autocomplete_waypoint = {
      input: ''
    };
    this.autocompleteItemsFrom = [];
    this.autocompleteItemsTo = [];
    this.autocompletewaypoint = [];

  }

  ionViewDidLoad() {
    this.initMap();
  }
  getCurrentLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
       var location = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+
                       resp.coords.latitude+","+resp.coords.longitude+"&key=AIzaSyAMi7bbbmmewyAcfg5_ATg-bjb8FCo7De4";
       this.http
      .get(location)
      .subscribe(
        data => {
          console.log(data);
          this.autocomplete_from.input = data['results'][0]['formatted_address'];
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );                
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }
  initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13,
      mapTypeId: 'roadmap'
    });


  }

  updateSearchResultsFrom(){
    if (this.autocomplete_from.input == '') {
      this.autocompleteItemsFrom = [];
      return;
    }

    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_from.input },
      (predictions, status) => {
        this.autocompleteItemsFrom = [];

        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItemsFrom.push(prediction);
            });
          });
        }
    });

    
  }

  add(){
    if(this.pricePerSeat < 500)
      this.pricePerSeat++;
  }
  sub(){
    if(this.pricePerSeat > 2)
      this.pricePerSeat--;
  }

  addFrom(i:any){
    if(this.stopOvers[i].price_from < 500)
      this.stopOvers[i].price_from++;
  }
  subFrom(i:any){
    if(this.stopOvers[i].price_from > 2)
      this.stopOvers[i].price_from--;
  }

  addTo(i:any){
    if(this.stopOvers[i].price_to < 500)
      this.stopOvers[i].price_to++;
  }
  subTo(i:any){
    if(this.stopOvers[i].price_to > 2)
      this.stopOvers[i].price_to--;
  }

  updateSearchResultsTo(){
    if (this.autocomplete_to.input == '') {
      this.autocompleteItemsTo = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_to.input },
      (predictions, status) => {
        this.autocompleteItemsTo = [];
        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItemsTo.push(prediction);

            });
          });
        }
    });
  }

  updateSearchResultsWaypoint(){
    if (this.autocomplete_waypoint.input == '') {
      this.autocompletewaypoint = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_waypoint.input },
      (predictions, status) => {
        this.autocompletewaypoint = [];
        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompletewaypoint.push(prediction);

            });
          });
        }
    });
  }


  selectSearchResultFrom(event: Event, item:any){

    this.autocomplete_from.input = item.description;
    this.autocompleteItemsFrom = [];
    document.getElementById("location_to").style.display = "block";
    
  }

  selectSearchResultTo(item){
    this.autocomplete_to.input = item.description;
    this.autocompleteItemsTo = [];

  }

  selectSearchResultWaypoint(event: Event, item:any){
    this.waypoints.push(item.description);
    this.autocompletewaypoint = [];
    this.autocomplete_waypoint = {
      input: ''
    };
  }

  Waypointremove(point, i){
    this.waypoints.splice(i, 1);
  }

  nextSlide() {
    this.slides.slideNext();
  }

  makeDirections(){
    this.startNavigating();
    this.hideMe = false;
    this.showMe = true;
  }

  startNavigating(){
        let now = moment().format('LLLL');

        let loader = this.loadingController.create({
            content: "Loading"
          });  
        loader.present();
        let directionsService = new google.maps.DirectionsService;
        let directionsDisplay = new google.maps.DirectionsRenderer;
        let waypts = [];

        directionsDisplay.setMap(this.map);

        for(var i=0; i < this.waypoints.length; i++){
            waypts.push({
              location: this.waypoints[i],
              stopover: true
            });
        }

        directionsService.route({
            origin: this.autocomplete_from.input,
            destination: this.autocomplete_to.input,
            travelMode: google.maps.TravelMode['DRIVING'],
            waypoints: waypts,
            optimizeWaypoints: true,
        }, (res, status) => {

            if(status == google.maps.DirectionsStatus.OK){
                directionsDisplay.setDirections(res);

                var last = res.routes[0].legs.length - 1;

                this.duration = 0;
                this.totalDistance = 0; 
                for(var j=0; j <= last; j++){
                  this.totalDistance = this.totalDistance + res.routes[0].legs[j].distance.value;
                  this.duration = this.duration + res.routes[0].legs[j].duration.value;
                }


                this.city_from = this.sliptCity(this.autocomplete_from.input);

                this.city_to = this.sliptCity(this.autocomplete_to.input);

                for(var i=0; i <= this.waypoints.length; i++){

                  var miles = res.routes[0].legs[i].distance.value * 0.000621371192;
                  var to_distance = (this.totalDistance - res.routes[0].legs[i].distance.value)* 0.000621371192 ;
                  
                  var price_from = Math.ceil( miles * this.global.pricePerMile);     
                  var price_to = Math.ceil( to_distance  * this.global.pricePerMile);

                  if(this.waypoints[i] != null){
                    var temp = this.waypoints[i];
                    var temp_name = temp.split(",");

                    var temp_from = res.routes[0].legs[i].start_address;
                    var from_array = temp_from.split(",");
                    var temp_to = res.routes[0].legs[i].end_address;
                    var to_array = temp_to.split(",");

                    this.stopOvers[i] = {
                      'from' : from_array[0],
                      'to' : to_array[0],
                      'end' : this.city_to,
                      'start' : this.city_from,
                      'city_address' : this.waypoints[i],
                      'city_name': temp_name[0],
                      'price_from' : price_from,
                      'price_to' : price_to,
                      'miles' : miles, 
                      'duration' : res.routes[0].legs[i].duration.value,
                    }
                 }
                } 


                this.totalDistance = Math.ceil(this.totalDistance * 0.000621371192);
                this.pricePerSeat = Math.ceil(this.totalDistance * this.global.pricePerMile);
                

                loader.dismiss();
            } else {
                const toast = this.toastCtrl.create({
                      message: 'Error trying to route, please try again',
                      duration: 2500,
                });
                toast.present();
                console.warn(status);
                loader.dismiss();
            }

        });

    }

    showSteps(directionResult) {
      // For each step, place a marker, and add the text to the marker's infowindow.
      // Also attach the marker to an array so we can keep track of it and remove it
      // when calculating new routes.
      var myRoute = directionResult.routes[0].legs[0];
      

    }
    
    sliptCity(city:any){
      var temp = city.split(",");

      if(temp.length == 3){
        return temp[0];
      }
      if(temp.length == 4){
        return temp[1];
      }
      else{
        return city;
      }

    }

    nextPage(){
      this.navCtrl.push('OneWaySecondPage', {
        duration : this.duration, 
        distance : this.totalDistance,
        price : this.pricePerSeat,
        location_from : this.autocomplete_from.input,
        location_to : this.autocomplete_to.input,
        waypoints:this.stopOvers,
        repeat: false,
      });
    }

}
