import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the UserReviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-reviews',
  templateUrl: 'user-reviews.html',
})
export class UserReviewsPage {
  currentUser:any;
  reviews:any;
  isEmpty:boolean = false;
  url:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public global : GlobalProvider,public http: HttpClient) {
    
  	this.url = this.global.url;
  	this.currentUser = this.navParams.get('user');
    this.getReviews();
  }

  ionViewDidLoad() {
    
  }

  getReviews(){
    var link = this.global.urlApi + "/getUserReviews/" + this.currentUser.user_id;

    this.http.get(link, 
    {
      headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),

    }).subscribe(data => {
        if(data['status'] = 1){
          this.reviews = data['reviews'];
           
        }

        if(this.reviews.length > 0){
          this.isEmpty=false;
        }else{
          this.isEmpty = true;
        }
        console.log(data);
    }, error => {
       console.log((error));
    });
  }

}
