import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserReviewsPage } from './user-reviews';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    UserReviewsPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(UserReviewsPage),
  ],
  exports:[UserReviewsPage]
})
export class UserReviewsPageModule {}
