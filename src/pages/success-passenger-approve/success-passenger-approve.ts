import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SuccessPassengerApprovePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-success-passenger-approve',
  templateUrl: 'success-passenger-approve.html',
})
export class SuccessPassengerApprovePage {
  response:any;
  status:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.response = navParams.get('data');
  	this.status = this.response.status;
  	console.log(this.response);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessPassengerApprovePage');
  }

}
