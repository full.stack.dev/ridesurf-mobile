import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccessPassengerApprovePage } from './success-passenger-approve';

@NgModule({
  declarations: [
    SuccessPassengerApprovePage,
  ],
  imports: [
    IonicPageModule.forChild(SuccessPassengerApprovePage),
  ],
  exports:[SuccessPassengerApprovePage]
})
export class SuccessPassengerApprovePageModule {}
