/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Nav, ModalController } from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { GlobalProvider } from '../../providers/global/global';
import { RideRemindersComponent } from '../../components/ride-reminders/ride-reminders';

//Action Sheets
import { ActionSheetController } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  currentUser;
  isDriver:boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController, 
    public http: HttpClient, 
    public global:GlobalProvider,
  ) 
  {

    this.currentUser = this.userProvider.currentUser()
      ? this.userProvider.currentUser()
      : JSON.parse(localStorage.getItem("userData"));

      if(this.currentUser.user.driver){
        this.isDriver = true;
        console.log(this.currentUser.user.driver)
      }
  }

  openRideReminders() {
     this.navCtrl.push('RideReminderListPage');
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SettingsPage");
  }

  codeCondut(){
    this.navCtrl.push('CodeOfConductPage');
  }

  changePassword() {
    this.navCtrl.push("ChangePasswordPage");
  }

  updatePaypal() {
    this.navCtrl.push("PaypalAccountPage");
  }

  deleteAccount() {
    this.navCtrl.push("DeleteAccountPage");
  }

  logoutClicked() {
    this.userProvider.makeLogout(this.currentUser.email);
    this.navCtrl.setRoot("BeforeLoginPage");
  }

  faqPage() {
    this.navCtrl.push("FaqPage");
  }

  helpPage() {
    this.navCtrl.push("HelpPage");
  }

  privacyPage() {
    this.navCtrl.push("PrivacyPage");
  }

  termPage() {
    this.navCtrl.push("TermsPage");
  }

  payments() {
    this.navCtrl.push("PaymentsPage");
  }

  carInfo() {
    this.navCtrl.push("CarInformationPage");
  }

  //--------------------------------------------
  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: "You will be redirected to our mobile website",
      buttons: [
        {
          text: "Go to FAQ",
          handler: () => {
            var ref = window.open(
              "https://ridesurf.io/faq",
              "_blank",
              "location=yes"
            );
            console.log("Go to FAQ clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }

  presentActionSheet2() {
    const actionSheet = this.actionSheetCtrl.create({
      title: "You will be redirected to our mobile website",
      buttons: [
        {
          text: "Get Help",
          handler: () => {
            var ref = window.open(
              "https://ridesurf.io/contact",
              "_blank",
              "location=yes"
            );
            console.log("Get Help clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
  //--------------------------------------------
}
