import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,  LoadingController , ToastController} from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';
import { RideHistoryPage } from '../ride-history/ride-history';
import { RidesBookedPage } from '../rides-booked/rides-booked';
/**
 * Generated class for the ReportProblemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report-problem',
  templateUrl: 'report-problem.html',
})
export class ReportProblemPage {

  rides_id:any;
  reason:any;
  body:any;
  currentUser:any;
  user_type:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController,
  			 public http: HttpClient,  public global : GlobalProvider, public loadingCtrl:LoadingController,
          public user:UserProvider) {

  	this.rides_id = this.navParams.get('rides_id');
    this.currentUser = this.user.currentUser();
    this.user_type = this.navParams.get('user_type');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportProblemPage');
  }

  submit(){
  	var link = this.global.urlApi + "/reportProblem";
  	let loader = this.loadingCtrl.create({
      content: "Sending..."
    });
    loader.present();
  	this.http.post(link,{
  		rides_id:this.rides_id,
  		reason:this.reason,
  		body:this.body,
  	},
  	{
        headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data  => {
          loader.dismiss();
          const toast = this.toastCtrl.create({
              message: data['message'],
              duration: 3500
            });
            toast.present();
            if(this.currentUser.driver == false){
              this.navCtrl.push( RidesBookedPage );
            }else{
              this.navCtrl.push( RideHistoryPage );
            }

      	}, error => {
         console.log(error);
         console.log("Oooops!");
    });
  }

}
