import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideCreatePage } from './ride-create';

@NgModule({
  declarations: [
    RideCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(RideCreatePage),
  ],
  exports:[RideCreatePage]
})
export class RideCreatePageModule {}
