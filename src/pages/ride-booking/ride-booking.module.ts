import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideBookingPage } from './ride-booking';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    RideBookingPage,
  ],
  imports: [
  StarRatingModule,
    IonicPageModule.forChild(RideBookingPage),
  ],
  exports:[RideBookingPage]
})
export class RideBookingPageModule {}
