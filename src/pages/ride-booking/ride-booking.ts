import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { AlertController } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the RideBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride-booking',
  templateUrl: 'ride-booking.html',
})
export class RideBookingPage {

  rideDetail:any;
  url:any;
  urlApi:any;
  seats: any = [];
  seatNum:any;
  request_description:any;
  dissable:boolean = false;
  currentUser:any;
  from:any;
  to:any;
  purpose:any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global : GlobalProvider,
    public alertCtrl:AlertController, public http: HttpClient, public loadingCtrl: LoadingController,
    public storage:Storage) {
    this.rideDetail = navParams.get('data');
    this.from = this.navParams.get('from');
    this.to = this.navParams.get('to');
    this.url = this.global.url;

    for(var i=0; i < this.rideDetail.seats_available; i++){
      this.seats.push( i+1 );
    }
    
    this.storage.get('userData').then((val) => {
          this.currentUser = val;
          if(this.currentUser.user_id == this.rideDetail.user_id){
            this.dissable = true;
          }
          console.log(this.dissable);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RideBookingPage');
  }

  bookNow(){
    if(this.seatNum == null){
      let prompt = this.alertCtrl.create({
            title: 'No Seats Selected',
            message: 'Please select one or more seats', 
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
      prompt.present();
    }else if(this.request_description == null){
      let prompt = this.alertCtrl.create({
            title: 'No Introduction',
            message: 'Please introduce yourself to your driver.', 
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });
      prompt.present();
    }else{
        let loader = this.loadingCtrl.create({
          content: "Loading..."
        });
        loader.present();

       var link = this.global.urlApi + "/requestJoin";
       
       this.http.post(link,{
          seats: this.seats,
          rideDetail : this.rideDetail.id,
          description:this.request_description,
          isStop:this.rideDetail.stop
        }, 
        {
          headers: new HttpHeaders()
                .set("Content-Type", "Application/json")
                .set("Accept", "application/json")
                .set('Authorization', 'Bearer '+ localStorage.getItem('token')),

        }).subscribe(data => {
          loader.dismiss();
           console.log('Success Booking');
           this.navCtrl.setRoot('SuccessRequestPage', {data : data});
      }, error => {
         loader.dismiss();
         let prompt = this.alertCtrl.create({
            title: 'Server Error',
            message: 'There was an error trying to book this ride', 
            buttons:
              [{
                  text: 'Close',
                  handler: data => {

                  }
              }]
          });

          prompt.present();
         console.log(error);
         console.log("Oooops!");
      });

    }

  }

  viewPublicProfile(user_id:any){

    var link  = this.global.urlApi + "/getPublicProfile/" + user_id;

    this.http.get(link,{
        headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           console.log(data);
           this.navCtrl.push( 'PublicProfilePage', { 
             user : data, 
             preferences: data['preferences'],
             sameProfile : false,
        })
         //localStorage.setItem('rideList' , data);
      }, error => {
         console.log("Oooops!");
      });
    
  }

}