import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ActionSheetController,
  ToastController,
  Platform,
  normalizeURL
} from "ionic-angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { GlobalProvider } from "../../providers/global/global";
import { UserProvider } from "../../providers/user/user";
import { Storage } from "@ionic/storage";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Events,AlertController } from "ionic-angular";
import { Dimensions, ImageCroppedEvent, ImageTransform } from 'ngx-image-cropper';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  currentUser: any;
  preferences: any;
  age: any = null;
  url: any;
  rating: any;
  imageChangedEvent: any = "";
  croppedImage: any = "";
  canvasRotation = 0;
  rotation = 0;
  scale = 1;
  transform: ImageTransform = {};
  profileImage: any;
  imageCrop: boolean = false;
  profileAvatar: any;
  getImage: boolean = false;
  response:boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public global: GlobalProvider,
    public http: HttpClient,
    public userProvider: UserProvider,
    public actionSheetCtrl: ActionSheetController,
    public storage: Storage,
    private camera: Camera,
    private platform: Platform,
    public toastCtrl: ToastController,
    public events: Events,
    public alertCtrl:AlertController,
  ) {

    this.url = this.global.url;
    this.userProvider.updateProfile();
    this.currentUser = this.userProvider.currentUser();
    this.profileAvatar = this.currentUser.avatar.startsWith('/') ? this.url + this.currentUser.avatar + "?" + new Date().getTime(): 
                         this.currentUser.avatar.startsWith('h') ? this.currentUser.avatar : "";
    this.rating = this.currentUser.user.rating;                     
    this.getThePreferences();
  }

  getProfileImage() {
    // let self = this;
    // var link = this.global.urlApi + "/getPreferenceOptions";
    // let loader = this.loadingCtrl.create({
    //   content: "Uploading..."
    // });
    
    // loader.present();
    setTimeout(()=>{
    this.userProvider.updateProfile();
    let self = this;
    this.userProvider.getProfileImage('/getProfilePic',function(resp){
      if(resp.status == 1) {
        //self.profileAvatar = self.url + resp.profile_pic+'?'+(new Date().getTime());
        self.profileAvatar = resp.profile_pic.startsWith('/') ? self.url + resp.profile_pic + "?" + new Date().getTime(): 
                             resp.profile_pic.startsWith('h') ? resp.profile_pic : "";
      } 
    })
    this.events.publish("updateUser", this.userProvider.currentUser());
    },1000);
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Profile photo uploaded',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  ionViewWillEnter(){
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    
    loader.present();
    setTimeout(()=>{
      this.currentUser = JSON.parse(localStorage.getItem("userData"));
      this.getThePreferences();
      console.log("User updated:  ", this.currentUser);
      loader.dismiss();
    },4000);
  }

  loadEditProfile() {
    this.navCtrl.push("ProfileEditPage");
  }


  editProfileImage() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Photo Library",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Camera",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 70,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 600
    };

    // Get the data of an image
    this.camera.getPicture(options).then(imagePath => {
      // Special handling for Android library
      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imagePath;
      this.profileImage = base64Image;
      this.imageCrop = true;
    });
  }

  imageCropped(image: string) {
    this.croppedImage = image;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

  rotateLeft() {
    this.canvasRotation--;
    this.flipAfterRotate();
  }

  rotateRight() {
    this.canvasRotation++;
    this.flipAfterRotate();
  }

  private flipAfterRotate() {
      const flippedH = this.transform.flipH;
      const flippedV = this.transform.flipV;
      this.transform = {
          ...this.transform,
          flipH: flippedV,
          flipV: flippedH
      };
  }

  flipHorizontal() {
      this.transform = {
          ...this.transform,
          flipH: !this.transform.flipH
      };
  }

  flipVertical() {
      this.transform = {
          ...this.transform,
          flipV: !this.transform.flipV
      };
  }

  updateRotation() {
    this.transform = {
        ...this.transform,
        rotate: this.rotation
    };
  }

  exitCropper(){
    this.imageCrop = false;
  }

  saveCroppedImage() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    
    loader.present();
    this.imageCrop = false;
    let self = this;
    let response = null;
    this.userProvider.imageUpload(
      "/uploadImageProfile",
      {
        vImage: this.croppedImage.base64
      },
      function(resp) {
        if (resp) {
          loader.dismiss();
          self.response = true;
          self.getProfileImage();
        }
      }
    );
  }

  editPreferences() {
    var link = this.global.urlApi + "/getPreferenceOptions";
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });

    loader.present();
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          loader.dismiss();
          this.navCtrl.push("PreferencesPage", {
            preferences: this.preferences,
            options: data
          });
        },
        error => {
          loader.dismiss();
          console.log(error);
        }
      );
  }

  getThePreferences() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

    loader.present();

    var link = this.global.urlApi + "/getTheUserPreferences";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.preferences = data;
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          console.log(error);
        }
      );
  }

  viewReview() {
    this.navCtrl.push("UserReviewsPage", { user: this.currentUser });
  }

  showPublicProfile() {
    this.navCtrl.push("PublicProfilePage", { 
      user: this.currentUser,
      same: true,
      preferences: this.preferences });
  }
}