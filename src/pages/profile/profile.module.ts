import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular'; 
import { CommonModule } from '@angular/common';
import { ProfilePage } from './profile';
import { StarRatingModule } from 'ionic3-star-rating';
import { ComponentsModule } from '../../components/components.module';
import { ImageCropperModule } from 'ngx-image-cropper';
 
@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
  	CommonModule,
  	ComponentsModule,
    StarRatingModule,
    ImageCropperModule,
    IonicPageModule.forChild(ProfilePage),
  ],
  exports:[ProfilePage, CommonModule]
})
export class ProfilePageModule {}
