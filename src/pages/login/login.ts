import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';
import { Events, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	credentialsForm: FormGroup;
  session:any;
  currentUser:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider : UserProvider,
  	private formBuilder: FormBuilder, public global : GlobalProvider,  public http: HttpClient,
     public events: Events, public storage : Storage, public fb: Facebook, public toastCtrl:ToastController) {

    this.session = navParams.get('session');

  	this.credentialsForm = this.formBuilder.group({
      email: [''],
      password: ['']
    });

  }

  onSignIn() {
    //Make the login with the user prodiver method
    this.userProvider.makeLogin(this.credentialsForm.value.email,
      this.credentialsForm.value.password, this.session);

  }

  register(){
    this.navCtrl.push('RegisterPage');
  }

  loginFacebook(){
 
    localStorage.clear();
    
    var facebook_user;
    // Login with permissions
    this.fb.login(['public_profile', 'email'])
    .then( (res: FacebookLoginResponse) => {

        // The connection was successful
        if(res.status == "connected") {
            // Get user ID and Token
            var fb_id = res.authResponse.userID;
            var fb_token = res.authResponse.accessToken;
            // Get user infos from the API
            this.fb.api("/me?fields=id,first_name,last_name,picture.type(large),gender,email", []).then((user) => {
                facebook_user = user;
                // Get the connected user details
                console.log('user info :');
                console.log(facebook_user);

               var link = this.global.urlApi + "/loginFacebook";

               this.http.post(link, {
                 first_name:user.first_name,
                 last_name:user.last_name,
                 id:user,
                 picture:user.picture,
               },
               { headers: new HttpHeaders()
                  .set('Accept', 'application/json ')

              }).subscribe(data => {

                console.log(data);
                if(data['action'] == 1){
                  this.currentUser = data;
                  this.storage.set('userData', data);
                  this.events.publish('user:login', data);
                  localStorage.setItem('token' , data['token']);
                  localStorage.setItem('userData' , JSON.stringify(data));
                  window["plugins"].OneSignal.sendTag("userid",data['user_id'])
                }else{
                  const toast = this.toastCtrl.create({
                    message: "Error trying to access Facebook",
                    duration: 3500
                  });

                   toast.present();
                }

              }, error => {
                 const toast = this.toastCtrl.create({
                    message: "There was an error connecting to our servers",
                    duration: 3500
                  });
                 toast.present();
                console.log(error);
              });

                // => Open user session and redirect to the next page

            });

        } 
        // An error occurred while loging-in
        else {
            const toast = this.toastCtrl.create({
                message: "There was a problem logging into your account, if the problem persists please contact us",
                duration: 3500
              });
             toast.present();
            console.log("An error occurred...");

        }

    })
    .catch((e) => {
          const toast = this.toastCtrl.create({
              message: "There was a problem logging into account, if the problem persists please contact us",
              duration: 3500
            });
          toast.present();
        console.log('Error logging into Facebook', e);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onForgotPassword(){
    this.navCtrl.push('ResetPasswordPage');
  }

}
