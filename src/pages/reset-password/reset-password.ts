import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController  } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  email:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public rest:RestProvider, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

  resetPassword(){
   
  	this.rest.resetPassword(this.email);
  	const toast = this.toastCtrl.create({
      message: 'We have sent an email to reset your password',
      duration: 3000
    });
    toast.present();
  	this.navCtrl.pop();
  }
}
