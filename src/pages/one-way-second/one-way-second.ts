import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Slides } from "ionic-angular";
import { FormBuilder, FormGroup } from "@angular/forms";
import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingController } from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { AlertController } from "ionic-angular";

/**
 * Generated class for the OneWaySecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-one-way-second",
  templateUrl: "one-way-second.html"
})
export class OneWaySecondPage {
  @ViewChild(Slides) slides: Slides;

  rideForm: FormGroup;
  currentUser: any;
  distance: any;
  duration: any;
  pricePerSeat: any;
  date: String = new Date().toISOString();
  ride: any;
  repeat: boolean = false;
  location_from: any;
  location_to: any;
  minDate: string = new Date().toISOString();
  maxDate: string = new Date("12/31/2050").toISOString();
  waypoints: any;
  isDisabled: boolean = true;
  ladies_only: boolean = false;
  purposes:any = [];
  luggageAmount:any=1;
  seats:any=1;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public http: HttpClient,
    public userProvider: UserProvider,
    public global: GlobalProvider,
    public loadingController: LoadingController,
    public alertCtrl: AlertController
  ) {
    this.ride = this.navParams.get("ride");
    this.repeat = this.navParams.get("repeat");
    this.distance = navParams.get("distance");
    this.location_from = this.navParams.get("location_from");
    this.location_to = this.navParams.get("location_to");
    this.waypoints = this.navParams.get("waypoints");
    this.pricePerSeat = this.navParams.get('price');

    this.currentUser = this.userProvider.currentUser();

    this.getPurposes();

    if (this.repeat == false) {
      this.rideForm = this.formBuilder.group({
        date: [""],
        seats: [""],
        price: [this.pricePerSeat],
        luggageSize: [""],
        luggageAmount: [""],
        time: [""],
        description: [""],
        womenOnly: [this.ladies_only],
        trip_purpose_id:[""]
      });
    } else {
      this.luggageAmount = this.ride.luggageAmount;
      this.seats = this.ride.seats;

      this.rideForm = this.formBuilder.group({
        date: [this.ride.date],
        price: [this.ride.price],
        luggageSize: [this.ride.luggageSize],
        time: [this.ride.startHour],
        description: [this.ride.description],
        womenOnly: [this.ladies_only],
        trip_purpose_id:[this.ride.trip_purpose_id]
      });
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad OneWaySecondPage");
  }

  getPurposes(){
    var link = this.global.urlApi + "/fetchPurposes";
    this.http.get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(res => {
         console.log(res);
         this.purposes = res;
       },error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  changeLadies($event) {
    if (this.ladies_only == false) {
      this.ladies_only = true;
    } else {
      this.ladies_only = false;
    }
  }

  nextSlide() {
    this.slides.slideNext();
  }

  allowContinue() {
    if(this.rideForm.value.date != ""){
      this.isDisabled = false;
    }
  }

  uploadRide() {

    if(this.rideForm.value.trip_purpose_id == ""){
      let prompt = this.alertCtrl.create({
            title: "Please complete the form",
            message: "Provide a trip purpose",
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
      prompt.present();
      return false;
    }
    if(this.rideForm.value.luggageSize == ""){
      let prompt = this.alertCtrl.create({
            title: "Please complete the form",
            message: "Provide a luggage size",
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
      prompt.present();
      return false;
    }
    if (this.ladies_only == true) {
      var ladies = "Yes";
    } else {
      var ladies = "No";
    }
    let loader = this.loadingController.create({
      content: "Loading"
    });
    loader.present();

    let Form = JSON.stringify(this.rideForm.value);
    console.log(this.pricePerSeat);

    var link = this.global.urlApi + "/createRide";
    var data = {
      dateFrom: this.rideForm.value.date,
      seats: this.rideForm.value.seats,
      price: this.pricePerSeat,
      luggage_size: this.rideForm.value.luggageSize,
      luggage_amount: this.luggageAmount,
      startHour: this.rideForm.value.time,
      description: this.rideForm.value.description,
      ladies_only: ladies,
      location_from: this.location_from,
      location_to: this.location_to,
      duration: this.navParams.get("duration"),
      seats_available: this.seats,
      waypoints: JSON.stringify(this.waypoints),
      trip_purpose_id:this.rideForm.value.trip_purpose_id
    };
    this.http
      .post(
        link,
        {
          data: data
        },
        {
          headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token")),
          observe: "body"
        }
      )
      .subscribe(
        data => {
          console.log(data);
          this.navCtrl.setRoot("SuccessCreateRidePage", { data: data["message"], type:0, });
          loader.dismiss();
        },
        error => {
          loader.dismiss();
          let prompt = this.alertCtrl.create({
            title: "Error",
            message: "Error " + error.error.message,
            buttons: [
              {
                text: "Close",
                handler: data => {}
              }
            ]
          });
          prompt.present();
          console.log(error);
        }
      );
  }


  addLuggage(){
    if(this.luggageAmount < 6)
      this.luggageAmount++;
  }

  subLuggage(){
    if(this.luggageAmount > 0)
      this.luggageAmount--;
  }

  addSeats(){
     if(this.seats < 5){
       this.seats++;
     }
  }

  subSeats(){
    if(this.seats > 1){
      this.seats--;
    }
  }
}
