import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OneWaySecondPage } from './one-way-second';

@NgModule({
  declarations: [
    OneWaySecondPage,
  ],
  imports: [
    IonicPageModule.forChild(OneWaySecondPage),
  ],
  exports:[OneWaySecondPage]
})
export class OneWaySecondPageModule {}
