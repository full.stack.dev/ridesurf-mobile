import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverApplyPage2Page } from './driver-apply-page2';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../components/components.module';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [
    DriverApplyPage2Page,
  ],
  imports: [
    CommonModule,
  	ComponentsModule,
  	ImageCropperModule,
    IonicPageModule.forChild(DriverApplyPage2Page),
  ],
  exports:[DriverApplyPage2Page]
})
export class DriverApplyPage2PageModule {}