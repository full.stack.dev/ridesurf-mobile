import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  App,
  Platform,
  ToastController,
  Slides
} from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { Base64 } from '@ionic-native/base64';
import { GlobalProvider } from '../../providers/global/global';
import { UserProvider } from '../../providers/user/user';
import { ImageTransform } from 'ngx-image-cropper';

@IonicPage()
@Component({
  selector: 'page-driver-apply-page2',
  templateUrl: 'driver-apply-page2.html'
})
export class DriverApplyPage2Page {
  @ViewChild(Slides) slides: Slides;
  imageBase64: any;
  options: CameraOptions = {
    quality: 60,
    sourceType: 1,
    saveToPhotoAlbum: false,
    correctOrientation: true,
    destinationType: this.camera.DestinationType.DATA_URL,
    targetHeight: 500
  };
  driverInfo: any;
  isDisabled: boolean = true;
  role: any;
  driver: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageCrop: boolean = false;
  canvasRotation = 0;
  rotation = 0;
  scale = 1;
  transform: ImageTransform = {};
  getImage: boolean = false;
  photoType: string;
  insurancePath: any;
  licencePath: any;
  licenseDisable:boolean = true;
  device:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public http: HttpClient,
    private camera: Camera,
    public userProvider: UserProvider,
    public toastCtrl: ToastController,
    public global: GlobalProvider,
    public base64: Base64,
    public app: App
  ) {
    this.driverInfo = navParams.get('driverInfo');
    this.driver = navParams.get('driver');
    this.role = navParams.get('role');
    if (this.role == 'Driver') {
      let userDriver = this.driver.user.driver[0];
      this.insurancePath = this.global.url + userDriver.insurance_path;
      this.licencePath = this.global.url + userDriver.driver_license;
    }

    if (this.platform.is('ios')) {
      this.device = 'ios';
    } else {
      this.device = 'android';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DriverApplyPage2Page');
  }

  continue() {
    this.slides.slideNext();
  }

  takePhoto(type) {
    this.photoType = type;
    this.camera.getPicture(this.options).then(imagePath => {
      this.imageBase64 = 'data:image/jpeg;base64,' + imagePath;
      this.imageCrop = true;
      if (this.photoType == 'insurance') {
          this.insurancePath = this.imageBase64;
        } else {
          this.licencePath = this.imageBase64;
        }
    });
  }

  /**
   * NGX Cropper w/ Rotation
   * @param image
   */
  imageCropped(image: string) {
    this.croppedImage = image;
    if(this.photoType == 'license'){
          this.slides.slideNext();
    }
  }

  imageLoaded() {
    console.log('image load:');
  }

  loadImageFailed() {
    console.log('image load failed:');
  }

  rotateLeft() {
    this.canvasRotation--;
    this.flipAfterRotate();
  }

  rotateRight() {
    this.canvasRotation++;
    this.flipAfterRotate();
  }

  private flipAfterRotate() {
    const flippedH = this.transform.flipH;
    const flippedV = this.transform.flipV;
    this.transform = {
      ...this.transform,
      flipH: flippedV,
      flipV: flippedH
    };
  }

  flipHorizontal() {
    this.transform = {
      ...this.transform,
      flipH: !this.transform.flipH
    };
  }

  flipVertical() {
    this.transform = {
      ...this.transform,
      flipV: !this.transform.flipV
    };
  }

  updateRotation() {
    this.transform = {
      ...this.transform,
      rotate: this.rotation
    };
  }

  exitCropper() {
    this.imageCrop = false;
  }

  saveCroppedImage() {
    let loader = this.loadingCtrl.create({
      content: 'Uploading...'
    });
    loader.present();
    this.imageCrop = false;
    let self = this;
    let url, params;
    if (this.photoType == 'insurance') {
      url = '/saveInsurance';
      params = {
        insurance: this.croppedImage.base64
      };
    } else {
      url = '/saveCarImage';
      params = {
        license: this.croppedImage.base64
      };
    }

    this.userProvider.imageUpload(url, params, function(res: any) {
       if (self.photoType == 'insurance') {
            self.isDisabled = false;
          } else {
            self.licenseDisable = false;
          }
      loader.dismiss();
      if (res && res.data) {
        let path = self.global.url + res.data;
        if (self.photoType == 'insurance') {
          self.insurancePath = path;
        } else {
          self.licencePath = path;
        }
      }
    });
  }

  nextPage() {
    this.navCtrl.push('DriverApplyPage3Page', {
      driverInfo: this.driverInfo,
      role: this.role,
      driver: this.driver
    });
  }
}
