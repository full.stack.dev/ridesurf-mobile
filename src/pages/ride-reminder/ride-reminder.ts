import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the RideReminderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ride-reminder',
  templateUrl: 'ride-reminder.html',
})
export class RideReminderPage {

  from:any;
  to:any;
  date:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global : GlobalProvider,
    		  public http: HttpClient, public toastCtrl: ToastController) {
  	this.from = this.navParams.get('from');
  	this.to = this.navParams.get('to');
  	this.date = this.navParams.get('date');


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RideReminderPage');
  }

  saveReminder(){
  	var link  = this.global.urlApi + "/saveRideReminder";

    this.http.post(link, {
    	from: this.from,
    	to: this.to,
    	date: this.date
    },
    {
          headers: new HttpHeaders()
            .set("Content-Type", "Application/json")
            .set("Accept", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
        }).subscribe(data => {
           const toast = this.toastCtrl.create({
	        message: 'Ride alert saved for ' + this.from + ' to ' + this.to,
	        duration: 4000
	      });
	      toast.present();
          this.navCtrl.pop();
         
      }, error => {
      	const toast = this.toastCtrl.create({
	        message: 'There was an error saving the reminder please try again another time.',
	        duration: 4000
	    });
	      toast.present();
         console.log("Oooops!");
      });
  }

}
