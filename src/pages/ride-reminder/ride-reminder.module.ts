import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RideReminderPage } from './ride-reminder';

@NgModule({
  declarations: [
    RideReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(RideReminderPage),
  ],
  exports:[RideReminderPage]
})
export class RideReminderPageModule {}
