import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events  } from 'ionic-angular';
import { GlobalProvider } from "../../providers/global/global";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
/**
 * Generated class for the CountryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-country-list',
  templateUrl: 'country-list.html',
})
export class CountryListPage {

  flagUrl:any;	
  countries:any;
  callback:any;
  area:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient,
  			  public global: GlobalProvider, private events: Events) {

  	this.flagUrl = this.global.url + "/flags/";

  	this.getCountries();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountryListPage');
  }

  ionViewWillEnter() {
	this.callback = this.navParams.get("callback")
  }

  returnToProfile(area:any) {
  		
	    this.navCtrl.pop().then(() => {
        /// Trigger custom event and pass data to be send back
        this.events.publish('get-area-code', area);
    });
  }

  getCountries(){
    var link = this.global.urlApi + "/countryCodes";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
      })
      .subscribe(
        data => {
          this.countries = data;
          var usa = {name: "United States", calling_code: "1", iso_3166_2: "US", flag: 'us.png'};
          this.countries.unshift(usa);
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

}
