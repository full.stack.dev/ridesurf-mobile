import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";

import { UserProvider } from "../../providers/user/user";
import { GlobalProvider } from "../../providers/global/global";

/**
 * Generated class for the PaypalAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-paypal-account",
  templateUrl: "paypal-account.html"
})
export class PaypalAccountPage {
  currentUser: any;
  paypalEmail: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public http: HttpClient,
    public global: GlobalProvider,
    private toastCtrl: ToastController
  ) {
    this.currentUser = this.userProvider.currentUser();
    this.paypalEmail = this.currentUser.paypal;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PaypalAccountPage");
  }

  updatePaypal() {
    var link = this.global.urlApi + "/updatePaypalAccount";

    this.http
      .post(
        link,
        {
          email: this.paypalEmail
        },
        {
          headers: new HttpHeaders()
            .set("Accept", "application/json")
            .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      )
      .subscribe(
        data => {
          console.log(data);
          let toast = this.toastCtrl.create({
            message: data["message"],
            duration: 3000
          });
          toast.present();
          this.userProvider.updateProfile();
          
        },
        error => {
          console.log(error);
        }
      );
  }
}
