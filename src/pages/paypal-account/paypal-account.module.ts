import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaypalAccountPage } from './paypal-account';

@NgModule({
  declarations: [
    PaypalAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(PaypalAccountPage),
  ],
  exports:[PaypalAccountPage]
})
export class PaypalAccountPageModule {}
