import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarInformationPage } from './car-information';

@NgModule({
  declarations: [
    CarInformationPage,
  ],
  imports: [
    IonicPageModule.forChild(CarInformationPage),
  ],
  exports:[
  	CarInformationPage
  ]
})
export class CarInformationPageModule {}
