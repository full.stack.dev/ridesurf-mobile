import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the CarInformationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-car-information',
  templateUrl: 'car-information.html',
})
export class CarInformationPage {
  
  carInfo:any;
  url:any;
  currentUser;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public userProvider: UserProvider,
  			  public http: HttpClient, public global:GlobalProvider, public loadingCtrl: LoadingController) {
  	this.getcarInfo();
  	this.url = this.global.url;

    this.currentUser = this.userProvider.currentUser();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CarInformationPage');
  }

  updateCarInfo(){
    this.navCtrl.setRoot('DriverApplyPage');
  }

  paypalAccount(){
    this.navCtrl.setRoot('PaypalAccountPage');
  }

  getcarInfo(){
  	let loader = this.loadingCtrl.create({
	    content: "Uploading..."
	  });
    let self = this;
	  loader.present();
    
  	var link = this.global.urlApi + "/getDriverInfo";
    this.http.get(link,
    {
      headers: new HttpHeaders()
            .set("content-type", "application/json")
            .set('Authorization', 'Bearer '+ localStorage.getItem('token')),
  
    }).subscribe(data => {
    	loader.dismiss();
    	this.carInfo = data;
    	console.log(data);
    }, error => {
    	loader.dismiss();
       this.carInfo = null;
       console.log(error);

    });

  }

}
