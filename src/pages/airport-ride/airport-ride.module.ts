import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AirportRidePage } from './airport-ride';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AirportRidePage,
  ],
  imports: [
  ComponentsModule,
    IonicPageModule.forChild(AirportRidePage),
  ],
  exports:[AirportRidePage]
})
export class AirportRidePageModule {}
