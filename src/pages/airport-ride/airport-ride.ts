import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { App } from 'ionic-angular';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { LoadingController,ToastController } from 'ionic-angular';
import { AlertController } from "ionic-angular";
import { GlobalProvider } from '../../providers/global/global';
import { RideHistoryPage } from '../../pages/ride-history/ride-history';

/**
 * Generated class for the AirportRidePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@IonicPage()
@Component({
  selector: 'page-airport-ride',
  templateUrl: 'airport-ride.html',
})

export class AirportRidePage {
  @ViewChild(Slides) slides: Slides;
  
  Airports:any;
  airportSelect:any;
  ride:any;
  geocoder:any;
  GooglePlaces:any;
  GoogleAutocomplete:any;
  autocomplete_from:any;
  location_from:any;
  autocompleteItemsFrom:any;
  map: any;
  destination:any;
  totlaPrice:any;
  page:any =1;
  isChild = true;
  continue:boolean = true;
  type:any;
  ride_date:any;
  minDate:any;
  ride_hour:any;
  meeting:any;
  slideNum:1;
  seats:any = 1;
  luggage_amount:any = 1;
  luggage_size:any = "Small";
  description:any;
  firstStep:boolean = true;
  secondStep:boolean = false;
  allowCreate:boolean = false;
  price:any;
  rideDuration:any;
  allowDay:boolean = true;
  allowHour:boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation,
  	public http: HttpClient,  public global : GlobalProvider, public app:App, public zone: NgZone,
    public loadingController: LoadingController, public toastCtrl: ToastController, 
    public alertCtrl: AlertController) {
    
  	this.Airports = this.navParams.get('airports');
  	console.log(this.Airports);
    this.minDate = new Date().toISOString();

  	this.geocoder = new google.maps.Geocoder;
    let elem = document.createElement("div");
    this.GooglePlaces = new google.maps.places.PlacesService(elem);
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();

    this.autocomplete_from = {
      input: 'Santa Clarita, CA, USA'
    };
    this.location_from = 'Santa Clarita, CA, USA';
    this.autocompleteItemsFrom = [];

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad AirportRidePage');
  }

  continueSearch(){
    this.continue = false;
  }

  changeAllowDay(){
    this.allowDay = false;
  }

  changeAllowHour(){
    this.allowHour = false;
  }

   getCurrentLocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
       var location = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+
                       resp.coords.latitude+","+resp.coords.longitude+"&key=AIzaSyAMi7bbbmmewyAcfg5_ATg-bjb8FCo7De4";
       this.http
      .get(location)
      .subscribe(
        data => {
          console.log(data);
          this.autocomplete_from.input = data['results'][0]['formatted_address'];
          this.location_from = data['results'][0]['formatted_address'];
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );                
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }

    initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13,
      mapTypeId: 'roadmap'
    });


  }
  
  updateSearchResultsFrom(){
    if (this.autocomplete_from.input == '') {
      this.autocompleteItemsFrom = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_from.input },
      (predictions, status) => {
        this.autocompleteItemsFrom = [];

        if(predictions){
          this.zone.run(() => {
            predictions.forEach((prediction) => {
              this.autocompleteItemsFrom.push(prediction);
            });
          });
        }
    });

    
  }

  selectSearchResultFrom(event: Event, item:any){

    this.autocomplete_from.input = item.description;
    this.autocompleteItemsFrom = [];
    this.location_from = item.description;
    console.log(this.location_from);
    //document.getElementById("location_from").style.display = "block";
    
  }

  sliptCity(city:any){
      var temp = city.split(",");

      if(temp.length == 3){
        return temp[0];
      }
      if(temp.length == 4){
        return temp[1];
      }
      else{
        return city;
      }

   }

   startNavigating(){
        let loader = this.loadingController.create({
            content: "Loading"
          });  
        loader.present();
        let directionsService = new google.maps.DirectionsService;
        let directionsDisplay = new google.maps.DirectionsRenderer;
        let waypts = [];

        directionsDisplay.setMap(this.map);

        directionsService.route({
            origin: this.autocomplete_from.input,
            destination: this.airportSelect.address + "," + this.airportSelect.city + "," + this.airportSelect.state,
            travelMode: google.maps.TravelMode['DRIVING'],
            waypoints: waypts,
            optimizeWaypoints: true,
        }, (res, status) => {

            if(status == google.maps.DirectionsStatus.OK){
                directionsDisplay.setDirections(res);
                console.log(res);
                
                var last = res.routes[0].legs.length - 1;

                var duration = 0;
                var totalDistance = 0; 

                for(var j=0; j <= last; j++){
                  totalDistance = totalDistance + res.routes[0].legs[j].distance.value;
                  duration = duration + res.routes[0].legs[j].duration.value;
                }

                this.rideDuration = duration;
                this.destination = this.sliptCity(this.autocomplete_from.input);

                var miles = totalDistance * 0.000621371192;
                
                this.totlaPrice = Math.ceil( miles * this.global.airportPrice);   
                if(this.totlaPrice < 10){
                  this.totlaPrice = 10;
                }else if(this.totlaPrice > 40 && this.totlaPrice < 75){
                   this.totlaPrice = this.totlaPrice * 0.85;
                }
                
                loader.dismiss();
            } else {
                const toast = this.toastCtrl.create({
                      message: 'Error trying to route, please try again',
                      duration: 2500,
                });
                toast.present();
                console.warn(status);
                loader.dismiss();
            }

        });

    }

  createRide(){

    this.allowCreate = true;
    var location = this.airportSelect.address +', ' 
    +this.airportSelect.city + ', ' + this.airportSelect.state + ', USA';

    if(this.seats < 0 || !this.seats){
      this.seats = 1;
    }

    var ride = {
      location_from: location,
      location_to: this.location_from,
      description:this.description,
      price:this.totlaPrice,
      seats:this.seats,
      seats_available:this.seats,
      luggage_amount:this.luggage_amount,
      luggage_size:this.luggage_size,
      dateFrom:this.ride_date,
      startHour:this.ride_hour,
      duration:this.rideDuration,
    }

    if(this.ride_date == null){
      const toast = this.toastCtrl.create({
          message: 'Please add a date to your ride',
          duration: 3500
        });
        toast.present();
        return false;
    }
    if(this.description == null){
        const toast = this.toastCtrl.create({
          message: 'Please add a description to your ride',
          duration: 3500
        });
        toast.present();
        return false;
    }
    if(this.ride_hour == null){
      const toast = this.toastCtrl.create({
          message: 'Please add a time to your ride',
          duration: 3500
        });
        toast.present();
        return false;
    }


    var link = this.global.urlApi + "/createAirportRide";
    let loader = this.loadingController.create({
      content: "Creating..."
    });
    loader.present();

    this.http.post(link,{
          ride:ride, 
          airportDetail:this.airportSelect

    },
    {
      headers: new HttpHeaders()
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + localStorage.getItem("token"))
        }
      ).subscribe(
        data => {
          loader.dismiss();
          this.secondStep = true;
          this.firstStep = false;
          
        },
        error => {
          loader.dismiss();
          this.allowCreate = false;
          const toast = this.toastCtrl.create({
              message: 'There was an error creating the ride',
              duration: 3500
            });
            toast.present();
          console.log(error);
        }
      );

  }


  nextSlide(){
    this.slideNum++;
    this.slides.slideNext();
  }

  backSlide(){
    this.slideNum--;
    this.slides.slidePrev();
  }

  add(number){
    if(this.totlaPrice < 250)
      this.totlaPrice++;
  }
  sub(number){
    if(this.totlaPrice > 5)
    this.totlaPrice--;
  }
  addSeats(){
    if(this.seats < 6)
      this.seats++;
  }
  subSeats(){
    if(this.seats > 1)
      this.seats--;
  }
  addLuggage(){
    if(this.luggage_amount < 6)
      this.luggage_amount++;
  }
  subLuggage(){
    if(this.luggage_amount > 1){
      this.luggage_amount--;
    }
  }

   rideHistory(){
    this.navCtrl.setRoot(RideHistoryPage, {tabIndex: 'currentRides'});
  }

}
