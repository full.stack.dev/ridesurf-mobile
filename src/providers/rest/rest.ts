import {
  HttpClient,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { Injectable, ViewChild } from "@angular/core";
import { GlobalProvider } from "../../providers/global/global";
import { Observable } from "rxjs/Observable";
import { LoadingController, App } from "ionic-angular";
import "rxjs/add/observable/interval";
import { UserProvider } from "../../providers/user/user";

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  link: any;
  currentUser: any;
  notifications: any;
  messages: any;
  messageCount: number;
  constructor(
    public http: HttpClient,
    public global: GlobalProvider,
    public userProvider: UserProvider,
    public loadingController: LoadingController,
    public app: App,
    public user:UserProvider
  ) {
    this.currentUser = this.userProvider.currentUser();
    this.link = this.global.urlApi;
  }

  // Sending a GET request to /notifications
  getNotifications(cb) {
    var link = this.link + "/getNotifications";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          cb(data);
        },
        error => {
          this.user.makeLogout(null);
          // let nav = this.app.getActiveNav();
          // nav.push('BeforeLoginPage');

          console.log(error);
          console.log("Oooops!");
        }
      );

    // return this.notifications;
  }

  getMessages() {
    var link = this.link + "/getNotificationMessages";
    var messages;
    let loader = this.loadingController.create({
      content: "Loading"
    });
    loader.present();
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          loader.dismiss();
          this.messages = data;
        },
        error => {
          loader.dismiss();
          console.log(error);
          console.log("Oooops!");
        }
      );

    loader.dismiss();
    return this.messages;
  }

  getMessageCount(cb) {
    var link = this.link + "/getMessagesCount";

    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      })
      .subscribe(
        data => {
          console.log(data);
          this.messageCount = data["count"];
          cb(data["count"]);
          console.log(this.messageCount);
        },
        error => {
          console.log(error);
          console.log("Oooops!");
        }
      );
  }

  resetPassword(email: any) {
    var link = this.global.urlApi + "/forgotPassword";

    this.http
      .post(
        link,
        {
          email: email
        },
        {
          headers: new HttpHeaders().set("Accept", "application/json")
        }
      )
      .subscribe(
        data => {
          console.log(JSON.stringify(data));
        },
        error => {
          console.log(JSON.stringify(error));
        }
      );
  }

  getNotificationCount(cd:any, cb:any){
    var url = this.global.urlApi + '/notificationCounts';
    
    this.http.get(url,
       {
        headers: new HttpHeaders()
          .set("Content-Type", "Application/json")
          .set("Accept", "application/json")
          .set("Authorization", "Bearer " + localStorage.getItem("token"))
      }).subscribe(
        data => {
           cd("Actions {" + data['alerts'] + "}");
           cb("Actions {" + data['actions'] + "}");
        },
        error => {

          console.log(error);
          console.log("Oooops!");
        });
  }
}
