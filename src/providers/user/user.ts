import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Events, AlertController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';

import { GlobalProvider } from '../../providers/global/global';

@Injectable()
export class UserProvider {
  _currentUser: any;

  constructor(
    public http: HttpClient,
    public global: GlobalProvider,
    public events: Events,
    public alertCtrl: AlertController,
    public storage: Storage,
    public toastCtrl: ToastController,
    public fb: Facebook,
    private sanitizer: DomSanitizer,
  ) {
    /* Clear user when changing servers*/
    //this.storage.set('userData', null);
    //this._currentUser = null;
    //this.storage.clear();
    this.storage.get('userData').then(val => {
      this._currentUser = val;
    });
    this._currentUser = JSON.parse(localStorage.getItem('userData'));
  }

  currentUser() {
    this.storage.get('userData').then(val => {
      if (val && val.avatar == null) {
        val.avatar = this.global.url + '/images/avatar.jpg';
      }
      return val;
    });
    return JSON.parse(localStorage.getItem('userData'));
  }

  makeLogin(email, password, session) {
    const link = this.global.urlApi + '/makeLogin';
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'multipart/form-data');
    let params = {
      Headers: headers,
      email: email,
      password: password
    };
    this.http.post(link, params).subscribe(
      data => {
        if (data['action'] == 1) {
          this._currentUser = null;
          localStorage.clear();
          //Get the information returning from the API
          //Set it to the local storage
          this._currentUser = data;
          //If the user does not have a profile image
          if (
            this._currentUser.avatar == '' ||
            this._currentUser.avatar == null
          ) {
            this._currentUser.avatar = '/images/avatar.jpg';
          }
          if (data != null) {
            console.log(data);
            this.storage.set('userData', data);
            this.events.publish('user:login', data);
            localStorage.setItem('token', data['token']);
            localStorage.setItem('userData', JSON.stringify(data));
            window['plugins'].OneSignal.sendTag('userid', data['user_id']);
          }
        } else if (data['action'] == 2) {
          let prompt = this.alertCtrl.create({
            title: 'Error',
            message: data['message'],
            buttons: [
              {
                text: 'Close',
                handler: data => {}
              }
            ]
          });
          prompt.present();
        }
      },
      error => {
        let prompt = this.alertCtrl.create({
          title: 'Error',
          message: 'Error 500 server out of service',
          buttons: [
            {
              text: 'Close',
              handler: data => {}
            }
          ]
        });
        prompt.present();
        console.log(error);
      }
    );
  }

  private getHeadersParam() {
    let headers = new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return { headers: headers };
  }

  saveProfileData(data) {
    const link = this.global.urlApi + '/saveProfileData';
    const params = {
      data: data
    };
    this.http.put(link, params, this.getHeadersParam()).subscribe(
      data => {
        this._currentUser = data['user'];
        this.storage.set('userData', data['user']);
        console.log(this._currentUser);
        let prompt = this.alertCtrl.create({
          title: 'Success',
          message: 'Profile successfully edited',
          buttons: [
            {
              text: 'Close',
              handler: data => {}
            }
          ]
        });
        prompt.present();
      },
      error => {
        console.log(JSON.stringify(error));
        let prompt = this.alertCtrl.create({
          title: 'Error',
          message: 'Error 500 on server',
          buttons: [
            {
              text: 'Close',
              handler: data => {}
            }
          ]
        });
        prompt.present();
      }
    );
  }

  makeLogout(email) {
    this.storage.set('userData', null);
    this._currentUser = null;
    this.storage.clear();

    localStorage.setItem('token', null);
    localStorage.clear();
    this.fb.getLoginStatus().then(res => {
      console.log(res.status);
      if (res.status === 'connected') {
        this.fb
          .logout()
          .then(res => {
            console.log('Facebook logout', res);
          })
          .catch(e => console.log('Error logging out from Facebook', e));
      }
    });
  }

  registerUser(data, res, message) {
    let link = this.global.urlApi + '/registerUser';
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'multipart/form-data');
    console.log(data);
    this.http
      .post(link, {
        Headers: headers,
        data: data
      })
      .subscribe(
        data => {
          if (data['action'] == 1) {
            this._currentUser = null;
            localStorage.clear();
            const toast = this.toastCtrl.create({
              message:
                'Succesfully registered account, please verify your email',
              duration: 3500
            });
            toast.present();
            this.storage.set('userData', data);
            this.events.publish('user:login', data);
            localStorage.setItem('token', data['token']);
            localStorage.setItem('userData', JSON.stringify(data));
            // window["plugins"].OneSignal.sendTag("userid",data['user_id'])
            res(true);
          } else {
            res(false);
            let alert = this.alertCtrl.create({
              title: 'Please fix the following error',
              message: data['message'],
              buttons: [
                {
                  text: 'Ok',
                  role: 'cancel',
                  handler: () => {
                    return false;
                  }
                }
              ]
            });
            alert.present();
          }
        },
        error => {
          let alert = this.alertCtrl.create({
            title: 'There was an error trying to register',
            message: error,
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {
                  return false;
                }
              }
            ]
          });
          alert.present();
          res(false);
        }
      );
  }

  resendVerificationEmail() {
    const link = this.global.urlApi + '/sendVerification';
    this.http.get(link, this.getHeadersParam()).subscribe(
      data => {
        let prompt = this.alertCtrl.create({
          title: 'Email Sent',
          message: data['message'],
          buttons: [
            {
              text: 'Close',
              handler: data => {
                return false;
              }
            }
          ]
        });
        prompt.present();
        console.log(JSON.stringify(data));
      },
      error => {
        console.log(JSON.stringify(error));
      }
    );
  }

  getDriverInfo() {
    let link = this.global.urlApi + '/getDriverInfo';
    this.http
      .get(link, {
        headers: new HttpHeaders()
          .set('Content-Type', 'x-www-form-urlencoded')
          .set('Accept', 'application/json')
          .set('Authorization', 'Bearer ' + localStorage.getItem('token'))
      })
      .subscribe(
        data => {
          alert(JSON.stringify(data));
          console.log(JSON.stringify(data));
        },
        error => {
          alert(JSON.stringify(error));
          console.log(JSON.stringify(error));
        }
      );
  }

  updatePreferences(data: any) {
    const link = this.global.urlApi + '/updatePreferences';
    let params = {
      data: data
    };
    this.http.post(link, params, this.getHeadersParam()).subscribe(
      data => {
        let prompt = this.alertCtrl.create({
          title: 'Preferences Updated',
          message: 'Your preferences have been saved',
          buttons: [
            {
              text: 'Close',
              handler: data => {}
            }
          ]
        });
        prompt.present();
      },
      error => {
        console.log(JSON.stringify(error));
        return 0;
      }
    );
  }

  changePassword(password: any, confirmPassword: any): any {
    const link = this.global.urlApi + '/changePassword';
    let params = {
      password: password,
      confirmPassword: confirmPassword
    };
    this.http.post(link, params, this.getHeadersParam()).subscribe(
      data => {
        let toast = this.toastCtrl.create({
          message: data['message'],
          duration: 3000
        });
        toast.present();
      },
      error => {
        let toast = this.toastCtrl.create({
          message: 'Error trying to change password',
          duration: 3000
        });
        toast.present();
        console.log(error);
      }
    );
    return true;
  }

  deleteAccount() {
    const link = this.global.urlApi + '/deleteAccount';
    this.http.get(link, this.getHeadersParam()).subscribe(
      data => {
        if (data['status'] == 200) {
          const toast = this.toastCtrl.create({
            message: data['message'],
            duration: 3500
          });
          toast.present();
          this._currentUser = null;
          this.storage.set('userData', null);
          this.storage.clear();
          localStorage.setItem('token', null);
          localStorage.clear();
        } else {
          const toast = this.toastCtrl.create({
            message: data['message'],
            duration: 3500
          });
          toast.present();
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  imageUpload(url, param, res) {
    console.log('in image uploader');
    const link = this.global.urlApi + url;
    this.http.post(link, param, this.getHeadersParam()).subscribe(
      data => {
        console.log(data);
        res(true);
      },
      error => {
        console.log(error);
        res(false);
      }
    );
  }

  updateProfile() {
    const link = this.global.urlApi + '/getUserInformation';
    this.http.get(link, this.getHeadersParam()).subscribe(
      data => {
        setTimeout(() => {
          if (data['avatar'] == null) {
            data['avatar'] = this.global.url + '/images/avatar.jpg';
          }
          this.storage.set('userData', data);
          localStorage.setItem('token', data['token']);
          localStorage.setItem('userData', JSON.stringify(data));
          this.events.publish('updateUser', data);
          this._currentUser = data;
        }, 1000);
      },
      error => {
        console.log(error);
      }
    );
  }

  getProfileImage(url, cb) {
    const link = this.global.urlApi + url;
    this.http.get(link, this.getHeadersParam()).subscribe(
      data => {
        this._currentUser.avatar = data['profile_pic'];
        cb(data);
      },
      error => {
        cb(error);
      }
    );
  }

  getCarInfo(cb) {
    const link = this.global.urlApi + '/getCarInfo';
    this.http.get(link, this.getHeadersParam()).subscribe(
      data => {
        cb(data);
      },
      error => {
        cb(error);
      }
    );
  }

  getTrustUrl(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
