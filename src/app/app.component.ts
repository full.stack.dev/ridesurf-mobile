import { Component, ViewChild } from "@angular/core";
import { Nav, Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { UserProvider } from "../providers/user/user";
import { Events } from "ionic-angular";
import { GlobalProvider } from "../providers/global/global";
import { Storage } from "@ionic/storage";
import { RestProvider } from "../providers/rest/rest";
import { Observable } from "rxjs/Observable";
import { NotValidatedComponent } from "../components/not-validated/not-validated";

import { HomePage } from "../pages/home/home";
import { NotificationsPage } from "../pages/notifications/notifications";
import { RideHistoryPage } from "../pages/ride-history/ride-history";
import { RidesBookedPage } from "../pages/rides-booked/rides-booked";

import "rxjs/add/observable/interval";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  currentUser: any = null;
  url: any;
  profilePages: Array<{ title: string; component: any }>;
  pages: Array<{ title: string; component: any; icon: string; num: any }>;
  years: Array<{ year: any }>;
  notifications: any;
  messageCount: any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public userProvider: UserProvider,
    public events: Events,
    public global: GlobalProvider,
    public storage: Storage,
    public rest: RestProvider
  ) {
    this.notifications = 0;
    this.messageCount = 0;

    this.url = this.global.url;

    events.subscribe("user:login", user => {
      // user is being pulled from userProvier on login
      // data is being sotre in localstorage
      this.storage.get("userData").then(val => {
        this.currentUser = this.userProvider.currentUser()
          ? this.userProvider.currentUser()
          : JSON.parse(localStorage.getItem("userData"));
        this.initApp();
        if (this.currentUser && this.currentUser.avatar == null) {
          this.currentUser.avatar = this.global.url + "/images/avatar.jpg";
        }

        this.nav.setRoot(HomePage);
      });
    });

    if (this.currentUser == null) {
      this.storage.get("userData").then(val => {
        this.currentUser = val;

        if (val != null) {
          this.initApp();
          if (this.currentUser.avatar == null) {
            this.currentUser.avatar = this.global.url + "/images/avatar.jpg";
          }
        }
      });
    }

    events.subscribe("updateUser", user => {
      this.currentUser = user;
      this.rest.getNotifications(notificationCount => {
        this.notifications = notificationCount;
        this.sideMenuPageRender();
      });
      this.rest.getMessageCount(messageCount => {
        this.messageCount = messageCount;
        this.sideMenuPageRender();
      });
    });

    
    events.subscribe("updateMessageCount", count => {
      
      this.rest.getNotifications(notificationCount => {
        this.notifications = notificationCount;
        this.sideMenuPageRender();
      });
      this.rest.getMessageCount(messageCount => {
        this.messageCount = messageCount;
        this.sideMenuPageRender();
      });
    });

    this.initializeApp();
  }

  sideMenuPageRender() {
    this.pages = [
      { title: "Home", component: HomePage, icon: "home", num: null },
      {
        title: "Find Ride",
        component: "RideSearchPage",
        icon: "custom-car",
        num: null
      },
      {
        title: "Offer Ride",
        component: "RideOfferPage",
        icon: "custom-drive",
        num: null
      },
      {
        title: "Offered Rides",
        component: RideHistoryPage,
        icon: "custom-calendar",
        num: null
      },
      {
        title: "Booked Rides",
        component: RidesBookedPage,
        icon: "md-bookmark",
        num: null
      },
      {
        title: "Notifications",
        component: NotificationsPage,
        icon: "md-notifications",
        num: this.notifications
      },
      {
        title: "Messages",
        component: "MessageListPage",
        icon: "md-mail",
        num: this.messageCount
      },
      {
        title: "Settings",
        component: "SettingsPage",
        icon: "md-cog",
        num: null
      }
    ];
  }

  initApp() {
    if (this.currentUser != null) {
      
      this.rest.getNotifications(notificationCount => {
        this.notifications = notificationCount;
        this.sideMenuPageRender();
      });
      this.rest.getMessageCount(messageCount => {
        this.messageCount = messageCount;
        this.sideMenuPageRender();
      });
      
      
    } else {
      this.messageCount = 0;
      this.notifications = 0;
      this.nav.setRoot("BeforeLoginPage");

    }

    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      let self = this;
      this.userProvider.updateProfile();
      this.currentUser = this.userProvider.currentUser();
      self.initApp();

      // One Signal to send message to notifications (push notifiactions)
      //Not working on IOS 
      /*
      if (this.platform.is("ios") || this.platform.is("android")) {
        // OneSignal Code start:
        // Enable to debug issues:
        // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

        var notificationOpenedCallback = function(jsonData) {
          console.log(
            "notificationOpenedCallback: " + JSON.stringify(jsonData)
          );
        };

        window["plugins"].OneSignal.startInit(
          "86536eae-8c27-4f95-af88-be81185300db",
          "ridesurf-590b1"
        )
          .handleNotificationOpened(notificationOpenedCallback)
          .handleNotificationReceived(function(jsonData) {
            console.log("RECEIVED ", jsonData);
            self.initApp();
          })
          .endInit();
      }*/
      
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    console.log("Open Page");
  }

  openProfile() {
    //Set root to profile page
    this.nav.setRoot("ProfilePage");
  }
}
