import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyApp } from './app.component';
import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from '@ionic-native/paypal';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { StatusBar } from '@ionic-native/status-bar';
import { StarRatingModule } from 'ionic3-star-rating';
import { Geolocation } from '@ionic-native/geolocation';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';
import { Crop } from '@ionic-native/crop';
import { Base64 } from '@ionic-native/base64';
import { ImageCropperModule } from 'ngx-image-cropper';
//Components
import { ComponentsModule } from '../components/components.module';

import { HomePage } from '../pages/home/home';
import { NotificationsPage } from '../pages/notifications/notifications';
import { RideHistoryPage } from '../pages/ride-history/ride-history';
import { RidesBookedPage } from '../pages/rides-booked/rides-booked';

//Proviers
import { UserProvider } from '../providers/user/user';
import { GlobalProvider } from '../providers/global/global';
import { RestProvider } from '../providers/rest/rest';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RideHistoryPage,
    RidesBookedPage,
    NotificationsPage
  ],
  exports: [],
  imports: [
    BrowserAnimationsModule,
    ComponentsModule,
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    StarRatingModule,
    ImageCropperModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      swipeBackEnabled: true,
      scrollAssist: true,
      autoFocusAssist: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RideHistoryPage,
    RidesBookedPage,
    NotificationsPage
  ],
  providers: [
    Geolocation,
    PasswordStrengthMeterModule,
    StatusBar,
    SplashScreen,
    PayPal,
    Facebook,
    Camera,
    File,
    FileTransfer,
    Crop,
    Base64,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserProvider,
    GlobalProvider,
    RestProvider
  ]
})
export class AppModule {}
